﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Targpiast.Controllers;
using Targpiast.Logic.Repositories;
using Targpiast.Models;

namespace Targpiast
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Grab information about the last error occurred 
            var exception = Server.GetLastError();

            // Clear the response stream 
            var httpContext = ((HttpApplication)sender).Context;
            httpContext.Response.Clear();
            httpContext.ClearError();
            httpContext.Response.TrySkipIisCustomErrors = true;

            // Manage to display a friendly view 
            InvokeErrorAction(httpContext, exception);
        }

        void InvokeErrorAction(HttpContext httpContext, Exception exception)
        {
            var routeData = new RouteData();
            routeData.Values["controller"] = "Home";
            routeData.Values["action"] = "Error";
            routeData.Values["exception"] = exception;
            using (var controller = new HomeController(new ErrorRepository(new ApplicationDbContext())))
            {
                ((IController)controller).Execute(
                    new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
        }
    }
}
