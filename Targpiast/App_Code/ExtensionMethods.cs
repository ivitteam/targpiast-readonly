﻿using System.Linq;
using Targpiast.Models;

namespace Targpiast
{
    public static class VideoExtensions
    {

        public static string VideoWatchUrl(this Video video)
        {
            return @"https://www.youtube.com/watch?v=" + video.VideoYtId();
        }

        public static string VideoTitleImage(this Video video)
        {
            return @"https://img.youtube.com/vi/" + video.VideoYtId() + @"/0.jpg";
        }

        private static string VideoYtId(this Video video)
        {
            return video.VideoUrl.Split('/').Last();
        }
    }
}
