﻿using System.ComponentModel.DataAnnotations;

namespace Targpiast.Models
{
  public class LoginViewModel
    {
        [Required]
        [Display(Name = "Użytkownik")]
        public string User { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }
    }
}
