﻿using System;

namespace Targpiast.Models
{
    public class ViewsModel
    {
        public DateTime DtDay { get; set; }
        public int IntSessions { get; set; }
        public int IntUsers { get; set; }
        public int IntNewUsers { get; set; }
        public int IntHits { get; set; }
    }

    public class NameValueModel
    {
        public string Name { get; set; }
        public int Views { get; set; }
    }
}