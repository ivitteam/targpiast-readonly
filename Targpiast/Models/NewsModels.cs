﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{

    public class News
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NewsId { get; set; }

        [Required]
        [DisplayName("Kategoria wiadomości")]
        public NewsCategory NewsCategory { get; set; }

        [Required]
        [DisplayName("Tytuł")]
        [StringLength(50)]
        public string NewsTitle { get; set; }

        [Required]
        [DisplayName("Zamieszczać na stronie tytułowej ?")]
        public bool NewsIncludeOnMain { get; set; }

        [DisplayName("Zdjęcie tytułowe")]
        [StringLength(40)]
        public string NewsTitlePhotoId { get; set; }

        [DisplayName("Skrócona treść wiadomości")]
        [StringLength(600)]
        public string NewsShortText { get; set; }

        [DisplayName("Treść wiadomości")]
        [MaxLength]
        public string NewsText { get; set; }

        [DisplayName("Data utworzenia")]
        public DateTime NewsCreatedDate { get; set; }

        [DisplayName("Data edycji")]
        public DateTime NewsEditedDate { get; set; }

        [NotMapped]
        public string NewsTitleOnMain => NewsTitle.Length > 30 ? $"{NewsTitle.Substring(0,30)}..." : NewsTitle;
    }

    public class NewsCategory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NewsCategoryId { get; set; }

        [Required]
        public string NewsCategoryName { get; set; }
    }

}