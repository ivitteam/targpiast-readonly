﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{
    public class Error
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ErrorId { get; set; }
        
        public string Hresult { get; set; }

        public string Message { get; set; }

        public string InnerException { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string TargetSite { get; set; }

        public DateTime Date { get; set; }

        public Error(Exception exception)
        {

            Hresult = exception.HResult.ToString();
            InnerException = exception.InnerException?.Message;
            Message = exception.Message;
            Source = exception.Source;
            StackTrace = exception.StackTrace;
            TargetSite = exception.TargetSite.ToString();
            Date = DateTime.Now;

        }
    }
}