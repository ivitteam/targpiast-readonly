﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        [Required]
        [DisplayName("Nazwa produktu")]
        [StringLength(50)]
        public string ProductName { get; set; }

        [Required]
        [DisplayName("Cena brutto")]
        [StringLength(20)]
        public string Price { get; set; }

        [Required]
        [DisplayName("Jednostka")]
        [StringLength(10)]
        public string Unit { get; set; }

        [Required]
        public virtual PriceList PriceList { get; set; }
    }

    public class PriceList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PriceListId { get; set; }

        [Required]
        [DisplayName("Data cennika")]
        public DateTime PriceListDateTime { get; set; } 

        public virtual ICollection<Product> Products { get; set; }
    }

}