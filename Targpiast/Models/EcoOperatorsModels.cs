﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{
	public class EcoOperator
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int OperatorId { get; set; }

		[Required]
		[DisplayName("Nazwa operatora")]
		[StringLength(150)]
		public string OperatorName { get; set; }

		[DisplayName("Lokalizacja")]
		[StringLength(100)]
		public string Localization { get; set; }

		[Required]
		[DisplayName("Asortyment")]
		[StringLength(120)]
		public string ProductCategory { get; set; }

		[Required]
		[DisplayName("Dane kontaktowe")]
		[StringLength(160)]
		public string ContactData { get; set; }

		[NotMapped]
		[DisplayName("Dane kontaktowe")]
		public string ContactDataWitUrls => Helpers.CodeHelpers.ReplaceContactTags(ContactData);
	}
}