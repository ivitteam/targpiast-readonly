﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{

    public class Operator
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OperatorId { get; set; }

        [Required]
        [DisplayName("Nazwa operatora")]
        [StringLength(150)]
        public string OperatorName { get; set; }
        
        [Required]
        [DisplayName("Numer stanowiska")]
        [StringLength(30)]
        public string ObjectNumber { get; set; }

        [Required]
        [DisplayName("Asortyment")]
        [StringLength(120)]
        public string ProductCategory { get; set; }

        [DisplayName("Dane kontaktowe")]
        [StringLength(160)]
        public string ContactData { get; set; }

        [NotMapped]
        [DisplayName("Dane kontaktowe")]
        public string ContactDataWitUrls => Helpers.CodeHelpers.ReplaceContactTags(ContactData);
    }

    public class Localization
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocalizationId { get; set; }

        [Required]
        [DisplayName("Nazwa lokalizacji")]
        [StringLength(60)]
        public string LocalizationName { get; set; }

        public ICollection<Operator> Operators { get; set; }
    }

}