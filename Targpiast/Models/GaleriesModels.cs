﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Targpiast.Models
{

    public class Photo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PhotoGalleryId { get; set; }

        [Required]
        [DisplayName("Nazwa galerii")]
        [StringLength(50)]
        public string PhotoGalleryName { get; set; }

        [Required]
        [DisplayName("Zamieszczać na stronie tytułowej ?")]
        public bool PhotoGalleryIncludeOnMain { get; set; }
    }

    public class Video
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VideoId { get; set; }

        [Required]
        [DisplayName("Nazwa filmu")]
        [StringLength(50)]
        public string VideoName { get; set; }

        [Required]
        [DisplayName("Zamieszczać na stronie tytułowej ?")]
        public bool VideoIncludeOnMain { get; set; }

        [Required]
        [DisplayName("Link z YouTube")]
        public string VideoUrl { get; set; }

        [NotMapped]
        public string VideoNameOnMain => VideoName.Length > 30 ? $"{VideoName.Substring(0, 30)}..." : VideoName;
    }

}