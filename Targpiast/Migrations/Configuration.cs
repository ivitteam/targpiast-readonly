using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Targpiast.Models;

namespace Targpiast.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //#region SEED NEWS
            //if (!context.NewsCategories.Any())
            //{
            //    context.NewsCategories.AddOrUpdate(x => x.NewsCategoryId,
            //        new NewsCategory { NewsCategoryId = 1, NewsCategoryName = "Aktualno�ci" },
            //        new NewsCategory { NewsCategoryId = 2, NewsCategoryName = "Eco Targpiast" }
            //    );
            //    context.SaveChanges();
            //}

            //if (!context.News.Any())
            //{
            //    context.News.AddOrUpdate(
            //           new News
            //           {
            //               NewsTitlePhotoId = "da3ca90619e44172b1fe6b90caed5871.png",
            //               NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //               NewsCreatedDate = new DateTime(2018, 3, 27),
            //               NewsEditedDate = new DateTime(2018, 3, 27),
            //               NewsIncludeOnMain = true,
            //               NewsShortText = "<p><br></p>",
            //               NewsText = "<p><br></p>",
            //               NewsTitle = "Nowy sektor handlowy"
            //           });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "c980308184204cd9a6fff4f63df28976.png",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2018, 3, 6),
            //            NewsEditedDate = new DateTime(2018, 3, 6),
            //            NewsIncludeOnMain = true,
            //            NewsShortText =
            //                "<p>Od marca br., zacznie obowi�zywa� ustawa ograniczaj�ca handel w niedziele. Zgodnie z jej zapisami (art.6 pkt 30 i 31), ograniczenie to nie obejmuje rynk�w hurtowych oraz podmiot�w prowadz�cych dzia�alno�� na rynkach hurtowych, czyli w TARGPIA�CIE nic si� nie zmieni.</p>",
            //            NewsText =
            //                "<p>Od marca br., zacznie obowi�zywa� ustawa ograniczaj�ca handel w niedziele. Zgodnie z jej zapisami (art.6 pkt 30 i 31), ograniczenie to nie obejmuje rynk�w hurtowych oraz podmiot�w prowadz�cych dzia�alno�� na rynkach hurtowych, czyli w TARGPIA�CIE nic si� nie zmieni.</p><p><br></p><p>Wiem, �e nie wszyscy -zw�aszcza osoby zatrudnione - s� z tego zadowoleni, jednak nie wszyscy wiedz�, �e w pierwotnym projekcie ustawy rygorystyczny zakaz handlu obejmowa� tak�e tzw. �inne czynno�ci sprzeda�owe�, czyli np. dostaw� towaru i kompletacj�. Oznacza�oby to, �e w przypadku wej�cia takich przepis�w w �ycie, Targpiast m�g�by uruchomi� wjazdy dla wszystkich pojazd�w : tych z towarem i tych po towar dopiero o 6.00 rano w poniedzia�ek. Nietrudno sobie wyobrazi�, jak w takiej sytuacji funkcjonowa�by rynek! Na szcz�cie ustawodawca uwzgl�dni� stanowisko Ministra Rolnictwa i Rozwoju Wsi wsparte g�osami �rodowisk rolniczych, producenckich i handlowych w tej sprawie, dlatego na rynkach hurtowych po wej�ciu w �ycie ustawy - handlujemy �po staremu� i kto wie, mo�e to dla wielu b�dzie szansa�?</p>",
            //            NewsTitle = "Targpiast otwarty w niedziele"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2017, 10, 11),
            //            NewsEditedDate = new DateTime(2017, 10, 11),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>8 pa�dziernika 2017 r. w Ogrodzie Botanicznym Uniwersytetu Wroc�awskiego odby� si� XIV FESTIWAL DYNI.</p>",
            //            NewsText = "<p>8 pa�dziernika 2017 r. w Ogrodzie Botanicznym Uniwersytetu Wroc�awskiego odby� si� XIV FESTIWAL DYNI.</p><p>Program obfitowa� w liczne atrakcje, w tym konkurs na najwi�ksz� dyni�. Targpiast razem z Ogrodem Botanicznym przygotowa� dla go�ci stoisko degustacyjne ze �wie�ymi warzywami i owocami z naszego rynku. Cieszy�o si� jak zwykle du�ym powodzeniem.</p>",
            //            NewsTitle = "XIV Festiwal Dyni"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "946caba2c8ee4affba06eaeade561196.jpg",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 2),
            //            NewsCreatedDate = new DateTime(2017, 9, 22),
            //            NewsEditedDate = new DateTime(2017, 9, 22),
            //            NewsIncludeOnMain = true,
            //            NewsShortText = "<p>Targpiast reaguje na trendy rynkowe zwi�zane z obrotem z szeroko rozumian� zdrow� �ywno�ci� � produktami regionalnymi, eco i �ywno�ci� certyfikowan�<br>Organizujemy now� platform� sprzeda�ow�, na kt�r� zapraszamy producent�w i operator�w zdrowej �ywno�ci.<br>Ju� dzi� na terenie Targpiastu dzia�a kilka firm, kt�rych profil jest zgodny z projektem ECO TARGPIAST.</p>",
            //            NewsText = "Targpiast reaguje na trendy rynkowe zwi�zane z obrotem z szeroko rozumian� zdrow� �ywno�ci� � produktami regionalnymi, eco i �ywno�ci� certyfikowan�<br>Organizujemy now� platform� sprzeda�ow�, na kt�r� zapraszamy producent�w i operator�w zdrowej �ywno�ci.<br>Ju� dzi� na terenie Targpiastu dzia�a kilka firm, kt�rych profil jest zgodny z projektem ECO TARGPIAST.<br>Zamierzamy dzia�a� na kilku p�aszczyznach:<br><ul><li>powierzchnia handlowa na terenie rynku wyodr�bniona cyklicznie dla produkt�w eco (targi, kiermasze, warsztaty i szkolenia dietetyczne); w przysz�o�ci sta�y element struktury handlowej rynku.</li><li>udost�pnienie przestrzeni wirtualnej Targpiastu dla promocji projektu i firm z nim zwi�zanych</li><li>udzia� w eventach zewn�trznych</li></ul>Projektem ECO TARGPIAST zarz�dza Anna P�awecka, z kt�r� mozna sie kontaktowa� telefonicznie: 601 751 647<br>",
            //            NewsTitle = "Eco Targpiast"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "26963d0f2c1b4b56be60a226d9e38a5e.jpg",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2017, 9, 12),
            //            NewsEditedDate = new DateTime(2017, 9, 12),
            //            NewsIncludeOnMain = true,
            //            NewsShortText = "<p>Organizowany od kilku lat Dzie� Targpiastu to �wi�to producent�w, handlowc�w, sprzedawc�w, najemc�w � wszystkich u�ytkownik�w rynku hurtowego Targpiast. Jest wyrazem szacunku i podzi�kowania dla wszystkich Klient�w za korzystania z naszego rynku.</p>",
            //            NewsText = "<p>Organizowany od kilku lat Dzie� Targpiastu to �wi�to producent�w, handlowc�w, sprzedawc�w, najemc�w � wszystkich u�ytkownik�w rynku hurtowego Targpiast. Jest wyrazem szacunku i podzi�kowania dla wszystkich Klient�w za korzystania z naszego rynku.</p><p>W tym roku Sp�ka zaprasza do �wi�tecznego stoiska na wprost bramy wjazdowej 14 wrze�nia (czwartek) godz. 05:00-12:00.<br>Na go�ci czeka� b�d�:<br></p><ul><li>bufet z aromatyczn� kaw�, herbat� i s�odkimi przek�skami</li><li>stoiska degustacyjne naszych operator�w</li><li>loteria, na kt�rej wszystkie losy wygrywaj�</li><li>prezentacja nowego projektu ECO Targpiast</li><li>wystawa fotografii studyjnych owoc�w i warzyw</li><li>stoisko pod has�em �drugie �ycie ksi��ki�</li><li>koncert zespo�u �1z12 akustyczne wrzenie�</li></ul><p>W hali og�lnospo�ywczej zainstaluje si� studio telewizji internetowej Pierwszego Portalu Rolnego.<br>Zapraszamy do wsp�lnej zabawy<br></p>",
            //            NewsTitle = "Dzie� Targpiastu 2017"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2016, 10, 7),
            //            NewsEditedDate = new DateTime(2016, 10, 7),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<div>Ogr�d Botaniczny Uniwersytetu Wroc�awskiego zaprasza na kolejny XIII Dolno�l�ski Festiwal Dyni , kt�ry odb�dzie 9 pa�dziernika 2016 r. na terenie Ogrodu Botanicznego we Wroc�awiu.</div>",
            //            NewsText = "<div>Ogr�d Botaniczny Uniwersytetu Wroc�awskiego zaprasza na kolejny XIII Dolno�l�ski Festiwal Dyni , kt�ry odb�dzie 9 pa�dziernika 2016 r. na terenie Ogrodu Botanicznego we Wroc�awiu.</div><div><br></div><div>W programie m.in. konkursy na najwi�ksz� i najdziwniejsz� dyni�, konkurs kulinarny, plener plastyczny, koncerty muzyczne. Organizatorzy zapraszaj� te� na kiermasz, na kt�rym b�dzie 80 stoisk spo�ywczych, artystycznych, pami�tkarskich i przemys�owych.</div><div><br></div><div>Jak co roku TARGPIAST zaprasza wszystkich go�ci festiwalu do swojego bezp�atnego, pe�nego warzyw i owoc�w stoiska degustacyjnego.</div>",
            //            NewsTitle = "XIII Dolno�l�ski Festiwal Dyni"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2016, 10, 5),
            //            NewsEditedDate = new DateTime(2016, 10, 5),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<div>15 wrze�nia 2016 r na terenie rynku obchodzili�my Dzie� Targpiastu -�wi�to u�ytkownik�w naszego rynku � najemc�w, handlowc�w i kupc�w. Zarz�dzaj�cy Targpiastem organizuj�c ten festyn chcieli wyrazi� szacunek i podzi�kowania Kupcom za prac� cz�sto ca�� dob� , za skuteczne zmagania z wyzwaniami dotycz�cymi handlu jak i z ca�� mas� nieprzewidywalnych zdarze� � jak cho�by warunki klimatyczne od kt�rych zale�y urodzaj.A przede wszystkim podzi�kowa� za tak liczn� obecno�� Kupc�w na naszym rynku, co pozwala dzia�a� nam ju� 26 lat, rozbudowywa� si� i ci�gle rozwija�.</div>",
            //            NewsText = "<div>15 wrze�nia 2016 r na terenie rynku obchodzili�my Dzie� Targpiastu -�wi�to u�ytkownik�w naszego rynku � najemc�w, handlowc�w i kupc�w. Zarz�dzaj�cy Targpiastem organizuj�c ten festyn chcieli wyrazi� szacunek i podzi�kowania Kupcom za prac� cz�sto ca�� dob� , za skuteczne zmagania z wyzwaniami dotycz�cymi handlu jak i z ca�� mas� nieprzewidywalnych zdarze� � jak cho�by warunki klimatyczne od kt�rych zale�y urodzaj.A przede wszystkim podzi�kowa� za tak liczn� obecno�� Kupc�w na naszym rynku, co pozwala dzia�a� nam ju� 26 lat, rozbudowywa� si� i ci�gle rozwija�.</div><div><br></div><div>W godz. 05:00-12:00 przy �wi�tecznym stoisku mo�na by�o napi� si� kawy, herbaty, zje�� s�odkiego smako�yka lub owoce, pos�ucha� dobrej muzyki i wzi�� udzia� w loterii, w kt�rej wszystkie losy wygrywa�y. Mo�na by�o te� obejrze� wystaw� fotograficzn� pt; �Targpiast od �witu � do �witu �� i przeczyta� najnowsze wydanie naszej gazety G�os Targpiastu.</div><div><br></div><div>Zapraszamy do obejrzenia zdj�� w Galerii.</div>",
            //            NewsTitle = "Dzie� Targpiastu 2016"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2015, 11, 6),
            //            NewsEditedDate = new DateTime(2015, 11, 6),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>6 listopada 2015 r. na terenie rynku, przed budynkiem biurowym stan�a wystawa fotograficzna pt. �Targpiast � spi�arnia Wroc�awia i Dolnego �l�ska� ilustruj�ca 25 lat naszej dzia�alno�ci.</p>",
            //            NewsText = "<p>6 listopada 2015 r. na terenie rynku, przed budynkiem biurowym stan�a wystawa fotograficzna pt. �Targpiast � spi�arnia Wroc�awia i Dolnego �l�ska� ilustruj�ca 25 lat naszej dzia�alno�ci.</p><p>We wrze�niu prezentowali�my j� na wroc�awskim placu Solnym.</p><p>36 fotogram�w z opisami opowiada o historii rynku i pokazuje bogactwo naszej oferty handlowej.</p><p>Serdecznie zapraszamy do jej obejrzenia wszystkich kupc�w, sprzedawc�w, go�ci �Targpiastu� i tych mieszka�c�w Wroc�awia i Dolnego �l�ska, kt�rzy nie zd��yli zrobi� tego wcze�niej.</p>",
            //            NewsTitle = "Wystawa Targpiast"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2015, 10, 9),
            //            NewsEditedDate = new DateTime(2015, 10, 9),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>Od dwunastu lat Ogr�d Botaniczny Uniwersytetu Wroc�awskiego zaprasza mieszka�c�w Wroc�awia, Dolnego �l�ska i go�ci z innych stron na Festiwal Dyni.</p>",
            //            NewsText = "<p>Od dwunastu lat Ogr�d Botaniczny Uniwersytetu Wroc�awskiego zaprasza mieszka�c�w Wroc�awia, Dolnego �l�ska i go�ci z innych stron na Festiwal Dyni.</p><p><br></p><p>W tym roku b�dzie si� odbywa� w niedziel� 11 pa�dziernika.&nbsp; Poznamy&nbsp; m.in. wyniki&nbsp; konkurs�w&nbsp; na dyni� najwi�ksz� i najdziwniejsz� oraz&nbsp; konkursu gotowania.&nbsp; Na go�ci czeka te� wiele innych atrakcji. Od kilku lat jedn� nich jest stoisko promocyjne �Targpiastu� , przy kt�rym&nbsp; b�dzie mo�na skosztowa� �wie�ych warzyw i owoc�w. Zapraszamy.</p>",
            //            NewsTitle = "XII Dolno�l�ski Festiwal Dyni"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2015, 9, 10),
            //            NewsEditedDate = new DateTime(2015, 9, 10),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>10 wrze�nia ju� od 5 rano na hadlowc�w czeka�a urodzinowa niespodzianka. Przy wje�dzie na rynek rozstawiony zosta� firmowy namiot �Targpiastu�, gdzie do godz. 12.00 serwowana b�dzie kawa, herbata , s�odkie �co nieco� , upominki firmowe oraz, wydana specjalnie na t� okazj�, gazeta jubileuszowa G�OS TARGPIASTU. Mo�na pos�ucha� dobrej muzyki i bardzo ciekawej audycji o naszym rynku. Kilka grup naszych pracownik�w i Ich dzieci chodzi�o po placu z upominkami dla najemc�w i sprzedawc�w ze stanowisk samochodowych.</p>",
            //            NewsText = "<p>10 wrze�nia ju� od 5 rano na hadlowc�w czeka�a urodzinowa niespodzianka. Przy wje�dzie na rynek rozstawiony zosta� firmowy namiot �Targpiastu�, gdzie do godz. 12.00 serwowana b�dzie kawa, herbata , s�odkie �co nieco� , upominki firmowe oraz, wydana specjalnie na t� okazj�, gazeta jubileuszowa G�OS TARGPIASTU. Mo�na pos�ucha� dobrej muzyki i bardzo ciekawej audycji o naszym rynku. Kilka grup naszych pracownik�w i Ich dzieci chodzi�o po placu z upominkami dla najemc�w i sprzedawc�w ze stanowisk samochodowych.</p><p>Relacja fotograficzna ze �wi�ta handlowc�w zamieszczona b�dzie jutro na naszej stronie w Galerii.</p>",
            //            NewsTitle = "Jubileusz Tarpiastu � trzecia ods�ona"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2015, 9, 8),
            //            NewsEditedDate = new DateTime(2015, 9, 8),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>4 wrze�nia br. �Targpiast� uroczy�cie �wi�towa� swoje 25-lecie. Na Gali Jubileuszowej w Teatrze Lalek spotkali si� pracownicy, handlowcy, przedstawiciele w�adz miejskich i samorz�dowych oraz �rodowisk bran�owych. Nie zabrak�o wsp�pracownik�w, kontrahent�w, przyjaci�.</p>",
            //            NewsText = "<p>4 wrze�nia br. �Targpiast� uroczy�cie �wi�towa� swoje 25-lecie. Na Gali Jubileuszowej w Teatrze Lalek spotkali si� pracownicy, handlowcy, przedstawiciele w�adz miejskich i samorz�dowych oraz �rodowisk bran�owych. Nie zabrak�o wsp�pracownik�w, kontrahent�w, przyjaci�.</p><p>Pracownikom i handlowcom najd�u�ej zwi�zanym z �Targpiastem� Zarz�d Sp�ki na scenie z�o�y� serdeczne podzi�kowania i uhonorowa� Ich pami�tkowymi dyplomami.</p><p>Go�cie z zainteresowaniem wys�uchali przem�wie� i obejrzeli film o �Targpia�cie�, a potem �wietnie si� bawili na spektaklu �Boenig, Boenig�.</p><p>Na bankiecie nie zabrak�o �urodzinowego� tortu i ch�ralnego �Sto lat�. Odbierali�my �yczenia i gratulacje. Wspominali�my stare czasy ogladaj�c na ekranie zdj�cia archiwalne.</p><p><br></p><p>Z okazji Jubileuszu na placu Solnym we Wroc�awiu stan�a wystawa fotograficzna prezentuj�ca histori� i dorobek �Targpiastu�.Serdecznie zapraszamy i zach�camy do obejrzenia ca�o�ci. Wystawa b�dzie czynna do 13 wrzesnia.</p>",
            //            NewsTitle = "Uroczysto�ci Jubileuszowe"
            //        });
            //    context.SaveChanges();

            //    context.News.AddOrUpdate(
            //        new News
            //        {
            //            NewsTitlePhotoId = "",
            //            NewsCategory = context.NewsCategories.Single(p => p.NewsCategoryId == 1),
            //            NewsCreatedDate = new DateTime(2015, 4, 8),
            //            NewsEditedDate = new DateTime(2015, 4, 8),
            //            NewsIncludeOnMain = false,
            //            NewsShortText = "<p>27 marca 1990 roku zosta�y podpisane dukumenty za�o�ycielskie Sp�ki WPPHU Targpiast Sp�ka z o.o., kt�rej celem by�o zbudowanie rolno-spo�ywczego rynku hurtowgo przy ul. Krzywoustego 126.</p>",
            //            NewsText = "<p>27 marca 1990 roku zosta�y podpisane dukumenty za�o�ycielskie Sp�ki WPPHU Targpiast Sp�ka z o.o., kt�rej celem by�o zbudowanie rolno-spo�ywczego rynku hurtowgo przy ul. Krzywoustego 126.</p><p>Po kilku latach zmieni�a si� struktura prawna i w�a�cielska sp�ki zarz�dzaj�cej a rynek zmieni� lokalizacj�.</p><p>Pocz�tki tej idei si�gaj� jeszcze s�ynnego placu Kromera, ale to w�a�nie w marcu 1990 roku w majestacie prawa, w gospodarczym pejza�u Wroc�awia pojawi� si� TARGPIAST. Dzi� funcjonuje przy ul. Obornickiej 235-237. Jest pierwszym w Polsce rynkiem hurtowym. Rol� spi�arni Wroc�awia, Dolnego �l�ska, Opolszczyzny i innych region�w o�ciennych pe�ni nieprzerwanie ju� od 25 lat!!!</p>",
            //            NewsTitle = "25-lecie TARPIASTU"
            //        }
            //    );
            //    context.SaveChanges();
            //}
            //#endregion

            //#region SEED GALLERIES
            //if (!context.Videos.Any())
            //{
            //    context.Videos.AddOrUpdate(x => x.VideoId,
            //        new Video() { VideoId = 1, VideoIncludeOnMain = true, VideoName = "Dzie� Targpiastu - Reporta�", VideoUrl = "https://youtu.be/3MmRKnNA0Kw" },
            //        new Video() { VideoId = 2, VideoIncludeOnMain = true, VideoName = "Targpiast - Spot reklamowy", VideoUrl = "https://youtu.be/KfrcGIAiuG4" },
            //        new Video() { VideoId = 3, VideoIncludeOnMain = true, VideoName = "FlashMob - Targpiast", VideoUrl = "https://youtu.be/qwqeqYdMgTQ" }
            //    );
            //    context.SaveChanges();
            //}

            //if (!context.Photos.Any())
            //{
            //    context.Photos.AddOrUpdate(x => x.PhotoGalleryId,
            //        new Photo { PhotoGalleryId = 1, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Wroc�awskie Dni Promocji Zdrowia 2010" },
            //        new Photo { PhotoGalleryId = 2, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "�wi�teczna zbi�rka �ywno�ci" },
            //        new Photo { PhotoGalleryId = 3, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Jubileusz 25-lecia" },
            //        new Photo { PhotoGalleryId = 4, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Dzie� Targpiastu 2016" },
            //        new Photo { PhotoGalleryId = 5, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Targpiast 2005" },
            //        new Photo { PhotoGalleryId = 6, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Targpiast 2010" },
            //        new Photo { PhotoGalleryId = 7, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Targpiast spi�arnia Wroc�awia i Dolnego �l�ska" },
            //        new Photo { PhotoGalleryId = 8, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "Dzie� Targpiastu 2017" },
            //        new Photo { PhotoGalleryId = 9, PhotoGalleryIncludeOnMain = true, PhotoGalleryName = "G�os Targpiastu" }
            //    );
            //    context.SaveChanges();
            //}
            //#endregion

            //#region SEED OPERATORS

            //if (!context.Localizations.Any())
            //{
            //    context.Localizations.AddOrUpdate(x => x.LocalizationId,
            //        new Localization()
            //        {
            //            LocalizationId = 1,
            //            LocalizationName = "HALA OG�LNOSPO�YWCZA NR 1",
            //            Operators = new List<Operator>
            //            {
            //                new Operator
            //                {
            //                    OperatorName = "PASEK S�AWOMIR",
            //                    ObjectNumber = "1-4",
            //                    ProductCategory = "Artyku�y og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "KRZY�ANOWSKI WIES�AW",
            //                    ObjectNumber = "16",
            //                    ProductCategory = "Artyku�y og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�WOJTEK�",
            //                    ObjectNumber = "34-38;78-80",
            //                    ProductCategory = "Artyku�y og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�JURAN�",
            //                    ObjectNumber = "40-47",
            //                    ProductCategory = "Artyku�y og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "MARCIIAK HELENA",
            //                    ObjectNumber = "70",
            //                    ProductCategory = "Artyku�y og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "WAJS JERZY",
            //                    ObjectNumber = "7",
            //                    ProductCategory = "Artku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "RUDO KRZYSZTOF",
            //                    ObjectNumber = "19-20",
            //                    ProductCategory = "Artku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "RAJTER MIECZYS�AW",
            //                    ObjectNumber = "81-83;83",
            //                    ProductCategory = "Artku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�KLEMENTYNKA-GOLD�S.S.",
            //                    ObjectNumber = "56-57;61",
            //                    ProductCategory = "Mleko przetwory mleczarskie",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�A.F.K.�",
            //                    ObjectNumber = "48-49;65-69",
            //                    ProductCategory = "Mleko przetwory mleczarskie",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�DROBASZ PLUS�",
            //                    ObjectNumber = "50-52",
            //                    ProductCategory = "Ryby i przetwory rybne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�NEPTUN�",
            //                    ObjectNumber = "92-98",
            //                    ProductCategory = "Ryby i przetwory rybne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�RYBIX�",
            //                    ObjectNumber = "119",
            //                    ProductCategory = "Ryby i przetwory rybne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�ELZA�",
            //                    ObjectNumber = "89",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�BRODA �s.c.",
            //                    ObjectNumber = "62-64",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�FENIX�",
            //                    ObjectNumber = "8-11",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�GOLDEN FRUIT�",
            //                    ObjectNumber = "87-88",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�FRUIT�GRZEGORZ JANKOWSKI",
            //                    ObjectNumber = "108-109",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = "[email=grzes.73@op.pl]"
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�KAMA�",
            //                    ObjectNumber = "90-91",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�KINIU�S.C.",
            //                    ObjectNumber = "60",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "F �KOKOS�",
            //                    ObjectNumber = "100-102",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "MAREK LECH",
            //                    ObjectNumber = "11O-113",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�PLMEX�",
            //                    ObjectNumber = "13-17",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�POLFRUIT�",
            //                    ObjectNumber = "21-27",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�WING�S.C.",
            //                    ObjectNumber = "84-85",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�ALMAR�",
            //                    ObjectNumber = "39",
            //                    ProductCategory = "Przet. owocowo -warzywne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "PTAK J�ZEF",
            //                    ObjectNumber = "74",
            //                    ProductCategory = "Przet. owocowo -warzywne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�KRZAKUS�",
            //                    ObjectNumber = "104",
            //                    ProductCategory = "Przet. owocowo -warzywne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�STEFIK�",
            //                    ObjectNumber = "76-77",
            //                    ProductCategory = "Groch, fasola ,mak",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "FP-H�TERESA�",
            //                    ObjectNumber = "24;114-116",
            //                    ProductCategory = "Opakowania art.  papiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "WOJSZAROWICZ",
            //                    ObjectNumber = "28",
            //                    ProductCategory = "Odzie�",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�LIMA�",
            //                    ObjectNumber = "54",
            //                    ProductCategory = "Wyroby tytoniowe",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�ARES�",
            //                    ObjectNumber = "107",
            //                    ProductCategory = "Wyroby tytoniowe",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�FLOCK-SYSTEM�",
            //                    ObjectNumber = "117",
            //                    ProductCategory = "Prasa ,lotto",
            //                    ContactData = ""
            //                }
            //            }
            //        });
            //    context.SaveChanges();

            //    context.Localizations.AddOrUpdate(x => x.LocalizationId,
            //        new Localization()
            //        {
            //            LocalizationId = 2,
            //            LocalizationName = "HALA OG�LNOSPO�YWCZA NR 2",
            //            Operators = new List<Operator>
            //            {
            //                new Operator
            //                {
            //                    OperatorName = "�ANTONIK�",
            //                    ObjectNumber = "223-225;228-229",
            //                    ProductCategory = "Art.og�lnispo�ycze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "MAZUR HENRYK",
            //                    ObjectNumber = "280",
            //                    ProductCategory = "Art.og�lnospo�ywcze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "PRYMA ANDRZEJ",
            //                    ObjectNumber = "205;210-213",
            //                    ProductCategory = "Artyku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "WY�UPEK IRENA",
            //                    ObjectNumber = "219-220",
            //                    ProductCategory = "Artyku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "K.F.D.�KRZYSZTOF�",
            //                    ObjectNumber = "244-247",
            //                    ProductCategory = "Arytku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "MAJOS PIOTR",
            //                    ObjectNumber = "255",
            //                    ProductCategory = "Arytku�y cukiernicze",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "KAZIMIEROWICZ ARTUR",
            //                    ObjectNumber = "230;258",
            //                    ProductCategory = "Mi�so ,w�dliny",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "Z-DY MI�SNE H.STOK�OSY",
            //                    ObjectNumber = "281-286",
            //                    ProductCategory = "Mi�so ,w�dliny",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�STEK-POL�",
            //                    ObjectNumber = "314-315",
            //                    ProductCategory = "Mi�so ,w�dliny",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�AMI�",
            //                    ObjectNumber = "231-232",
            //                    ProductCategory = "Dr�b i podroby",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�PIRO�",
            //                    ObjectNumber = "201;221-222",
            //                    ProductCategory = "Mleko i przetw. mleczarskie",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�FISCHLAND�",
            //                    ObjectNumber = "303-304",
            //                    ProductCategory = "Ryby i przetwory rybne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "CHATYS  STANIS�AW",
            //                    ObjectNumber = "202-203",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�ARNA � BIS� T. �urawki",
            //                    ObjectNumber = "214,242-243",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = " LICHOTA EL�BIETA",
            //                    ObjectNumber = "251",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "FOL-PAK OPAKOWANIA CHEMIA  Roman Majcher",
            //                    ObjectNumber = "252-253",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "DZIEWCZY�SKI HENRYK",
            //                    ObjectNumber = "273",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�MAJKA�",
            //                    ObjectNumber = "305-313",
            //                    ProductCategory = "Art. papiernicze, gospodarstwa domowego i chemii gospod.",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�WUJA�",
            //                    ObjectNumber = "215-218;250,317",
            //                    ProductCategory = "Napoje i przetwory owocowo-warzywne",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�DOMINO�P.H.U.",
            //                    ObjectNumber = "236-239;264-267",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�FRESH�",
            //                    ObjectNumber = "268-272;287-296;316",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�GOLDEN FRUIT�",
            //                    ObjectNumber = "233-235;259-263",
            //                    ProductCategory = "Owoce, warzywa importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�JAGA�",
            //                    ObjectNumber = "206-207",
            //                    ProductCategory = "Alkohole do 18%, piwo",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�AKAR�",
            //                    ObjectNumber = "301",
            //                    ProductCategory = "Wyroby tytoniowe",
            //                    ContactData = ""
            //                }
            //            }
            //        });
            //    context.SaveChanges();
            //    context.Localizations.AddOrUpdate(x => x.LocalizationId,
            //        new Localization()
            //        {
            //            LocalizationId = 3,
            //            LocalizationName = "HALA HC-1",
            //            Operators = new List<Operator>
            //            {
            //                new Operator
            //                {
            //                    OperatorName = "PHU �KRISELPOL�",
            //                    ObjectNumber = "4",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "FIRMA HANDLOWO-US�UGOWA W�cleski Arkadiusz",
            //                    ObjectNumber = "5",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "HANDEL OBWO�NY ARTYKU�AMI ROLNO-SPO�YWCZYMI Cie�la Marek",
            //                    ObjectNumber = "7",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "HANDEL HURTOWY I DETALICZNY Zmy�lony  Marian",
            //                    ObjectNumber = "10",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "PPHU �REMIKO� R.MICHALSKI",
            //                    ObjectNumber = "13 ",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "PRZEDSI�BIORSTWO PHU IMPORT EXPORT Roma�ski  Jerzy",
            //                    ObjectNumber = "14",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName =
            //                        "SKUP I SPRZEDA� ARTYKU��W SPO�YWCZYCH I PRZEMYS�OWYCH  E. S�owik, R. Zieli�ski",
            //                    ObjectNumber = "15",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�BYSPOL� Janusz Bystry",
            //                    ObjectNumber = "20",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "PPHU�REMIKO�R.Michalski",
            //                    ObjectNumber = "24",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�BENPOL�F.H.U. Eugeiusz Radziuk",
            //                    ObjectNumber = "6-31",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "SZUMSKI  MAREK",
            //                    ObjectNumber = "36",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                }
            //            }
            //        });
            //    context.SaveChanges();
            //    context.Localizations.AddOrUpdate(x => x.LocalizationId,
            //        new Localization()
            //        {
            //            LocalizationId = 4,
            //            LocalizationName = "HALA HC-2",
            //            Operators = new List<Operator>
            //            {
            //                new Operator
            //                {
            //                    OperatorName = "F.H.U.�BENPOL�R.Raziuk",
            //                    ObjectNumber = "3073",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�BYSPOL� Janusz Bystry",
            //                    ObjectNumber = "3074",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�JAGA�",
            //                    ObjectNumber = "3075",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "P.P.U.�PAW-MAG�IMPORT EXPORTS.C. Wnuk Piotr, Kaczor Jacek",
            //                    ObjectNumber = "3076",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�SAWPOL� Sawa Grzegorz",
            //                    ObjectNumber = "3077",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�POLMEX� SP. Z O.O.",
            //                    ObjectNumber = "3078",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�SWETT�PPHU",
            //                    ObjectNumber = "3079 - 3080",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                },
            //                new Operator
            //                {
            //                    OperatorName = "�EDI�",
            //                    ObjectNumber = "3081",
            //                    ProductCategory = "Warzywa i owoce importowane",
            //                    ContactData = ""
            //                }
            //            }
            //        }
            //    );
            //    context.SaveChanges();
            //}

            //#endregion

            //#region SEED PRICE LISTS

            //if (!context.PriceLists.Any())
            //{
            //    context.PriceLists.AddOrUpdate(x => x.PriceListId,
            //        new PriceList
            //        {
            //            PriceListDateTime = new DateTime(2018, 02, 15, 13, 50, 00),
            //            Products = new List<Product>
            //            {
            //                new Product {ProductName = "Ananasyyyy", Price = "7.00-8.00", Unit = "sztuka"},
            //                new Product {ProductName = "Banany", Price = "5.50-6.00", Unit = "kg"},
            //                new Product {ProductName = "Boczniaki", Price = "2.50-3.00", Unit = "200g"},
            //                new Product {ProductName = "Broku�y imp.", Price = "-", Unit = "szt"},
            //                new Product {ProductName = "Brukselka", Price = "5", Unit = "kg"},
            //                new Product {ProductName = "Buraki", Price = "1.00-1.20", Unit = "kg"},
            //                new Product {ProductName = "Cebula", Price = "0.8", Unit = "kg"},
            //                new Product {ProductName = "Cebula czerwona", Price = "1.60-2.00", Unit = "kg"},
            //                new Product {ProductName = "Chrzan", Price = "7.00-8.00", Unit = "kg"},
            //                new Product {ProductName = "Cukinia", Price = "6", Unit = "kg"},
            //                new Product {ProductName = "Cytryna", Price = "6.4", Unit = "kg"},
            //                new Product {ProductName = "Czosnek", Price = "1.00-2.00", Unit = "sztuka"},
            //                new Product {ProductName = "Fasola (drobna)", Price = "6", Unit = "kg"},
            //                new Product {ProductName = "Fasola ja� (�rednia)", Price = "5.00-6.00", Unit = "kg"},
            //                new Product {ProductName = "Fasola ja� du�a (chi�ska)", Price = "10", Unit = "kg"},
            //                new Product {ProductName = "Fasola ja� du�a (polska)", Price = "12", Unit = "kg"},
            //                new Product {ProductName = "Grapefruity czerwone", Price = "6", Unit = "kg"},
            //                new Product {ProductName = "Grapefruity Florida", Price = "15", Unit = "kg"},
            //                new Product {ProductName = "Groch nie�uskany", Price = "2", Unit = "kg"},
            //                new Product {ProductName = "Groch �uskany", Price = "2.2", Unit = "kg"},
            //                new Product {ProductName = "Gruszki import", Price = "5.00-5.00", Unit = "kg"},
            //                new Product {ProductName = "Gruszki krajowe", Price = "2.20-4.00", Unit = "kg"},
            //                new Product {ProductName = "Jab�ka krajowe", Price = "1.60-3.70", Unit = "kg"},
            //                new Product {ProductName = "Jajka M,L", Price = "0.50-0.55", Unit = "sztuka"},
            //                new Product {ProductName = "Jajka XL", Price = "0.55-0.65", Unit = "sztuka"},
            //                new Product {ProductName = "Kalafiory import", Price = "4.00-5.00", Unit = "szt."},
            //                new Product {ProductName = "Kalarepa", Price = "2", Unit = "szt."},
            //                new Product {ProductName = "Kapusta bia�a", Price = "0.40-0.50", Unit = "kg"},
            //                new Product {ProductName = "Kapusta kiszona", Price = "2.3", Unit = "kg"},
            //                new Product {ProductName = "Kapusta peki�ska", Price = "1.60-2.00", Unit = "kg"},
            //                new Product {ProductName = "Kapusta w�oska", Price = "4.00-4.50", Unit = "sztuka"},
            //                new Product {ProductName = "Kiwi", Price = "0.80-1.00", Unit = "sztuka"},
            //                new Product {ProductName = "Koperek", Price = "1.00-1.50", Unit = "p�czek"},
            //                new Product {ProductName = "Mak", Price = "12", Unit = "kg"},
            //                new Product {ProductName = "Mandarynki", Price = "6.00-7.00", Unit = "kg"},
            //                new Product {ProductName = "Marchew", Price = "1.00-1.40", Unit = "kg"},
            //                new Product {ProductName = "Na� pietruszki", Price = "0.80-1.20", Unit = "p�czek"},
            //                new Product {ProductName = "Og�rki kiszone", Price = "5.00-5.30", Unit = "kg"},
            //                new Product {ProductName = "Og�rki szklarniowe import", Price = "6.50-12.00", Unit = "kg"},
            //                new Product {ProductName = "Og�rki szklarniowe krajowe", Price = "12", Unit = "kg"},
            //                new Product
            //                {
            //                    ProductName = "Orzechy laskowe nie�uskane",
            //                    Price = "10.00-12.00",
            //                    Unit = "kg"
            //                },
            //                new Product {ProductName = "Orzechy laskowe �uskane", Price = "30", Unit = "kg"},
            //                new Product {ProductName = "Orzechy w�oskie nie�uskane", Price = "8", Unit = "kg"},
            //                new Product {ProductName = "Orzechy w�oskie �uskane", Price = "34.00-37.00", Unit = "kg"},
            //                new Product {ProductName = "Papryka czerwona", Price = "8.00-8.80", Unit = "kg"},
            //                new Product {ProductName = "Papryka zielona", Price = "10", Unit = "kg"},
            //                new Product {ProductName = "Papryka ��ta", Price = "10", Unit = "kg"},
            //                new Product {ProductName = "Pieczarki", Price = "5.00-5.50", Unit = "kg"},
            //                new Product {ProductName = "Pietruszka", Price = "3.6", Unit = "kg"},
            //                new Product {ProductName = "Pomara�cze", Price = "4", Unit = "kg"},
            //                new Product {ProductName = "Pomidory", Price = "6.00-6.50", Unit = "kg"},
            //                new Product {ProductName = "Pomidory malinowe krajowe", Price = "20.00-24.00", Unit = "kg"},
            //                new Product {ProductName = "Por", Price = "3", Unit = "kg"},
            //                new Product {ProductName = "Por", Price = "1.20-1.50", Unit = "szt."},
            //                new Product
            //                {
            //                    ProductName = "Ryby - brzuszki z �ososia w�dzone 2+",
            //                    Price = "19",
            //                    Unit = "kg"
            //                },
            //                new Product {ProductName = "Ryby - czarniak w�dzony", Price = "-", Unit = "kg"},
            //                new Product {ProductName = "Ryby - dorsz atlantycki w�dzony", Price = "20", Unit = "kg"},
            //                new Product
            //                {
            //                    ProductName = "Ryby - filet z czarniaka SHP b/s mro�ony",
            //                    Price = "22",
            //                    Unit = "kg"
            //                },
            //                new Product
            //                {
            //                    ProductName = "Ryby - filet z mintaja SHP b/s mro�ony",
            //                    Price = "10.4",
            //                    Unit = "kg"
            //                },
            //                new Product {ProductName = "Ryby - filet �ledziowy a la matjas", Price = "11", Unit = "kg"},
            //                new Product {ProductName = "Ryby - filet �ledziowy po wiejsku", Price = "16", Unit = "kg"},
            //                new Product {ProductName = "Ryby - karp patroszony", Price = "22", Unit = "kg"},
            //                new Product {ProductName = "Ryby - karp �ywy", Price = "16", Unit = "kg"},
            //                new Product {ProductName = "Ryby - makrela w�dzona 300/500", Price = "11.5", Unit = "kg"},
            //                new Product {ProductName = "Ryby-pstr�g �wie�y (ca�y)", Price = "21", Unit = "kg"},
            //                new Product {ProductName = "Ryby - szprot w�dzony", Price = "10", Unit = "kg"},
            //                new Product {ProductName = "Ryby - �led� solony tusza", Price = "9", Unit = "kg"},
            //                new Product {ProductName = "Rzodkiew bia�a", Price = "3.2", Unit = "kg"},
            //                new Product {ProductName = "Rzodkiewka", Price = "1.4", Unit = "p�czek"},
            //                new Product {ProductName = "Sa�ata import", Price = "2.00-2.60", Unit = "szt."},
            //                new Product {ProductName = "Sa�ata lodowa import", Price = "3", Unit = "sztuka"},
            //                new Product {ProductName = "Seler", Price = "3", Unit = "kg"},
            //                new Product {ProductName = "Szczaw", Price = "-", Unit = "p�czek"},
            //                new Product {ProductName = "Szczypior", Price = "1.6", Unit = "p�czek"},
            //                new Product {ProductName = "Szpinak", Price = "-", Unit = "kg"},
            //                new Product {ProductName = "Winogrona bia�e", Price = "15.5", Unit = "kg"},
            //                new Product {ProductName = "Winogrona czerwone", Price = "14", Unit = "kg"},
            //                new Product {ProductName = "W�oszczyzna", Price = "3.00-3.50", Unit = "p�czek"},
            //                new Product {ProductName = "W�oszczyzna", Price = "1.20-1.50", Unit = "tacka"},
            //                new Product {ProductName = "Ziemniaki krajowe", Price = "0.50-0.60", Unit = "kg"},
            //                new Product {ProductName = "�urawina suszona", Price = "15.00-24.00", Unit = "kg"}

            //            }
            //        }
            //    );
            //    context.SaveChanges();
            //}

            //#endregion
        }
    }
}
