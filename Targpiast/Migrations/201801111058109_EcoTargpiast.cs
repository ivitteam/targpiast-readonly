namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EcoTargpiast : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EcoOperators",
                c => new
                    {
                        OperatorId = c.Int(nullable: false, identity: true),
                        OperatorName = c.String(nullable: false, maxLength: 50),
                        Localization = c.String(nullable: false, maxLength: 100),
                        ProductCategory = c.String(nullable: false, maxLength: 100),
                        ContactData = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.OperatorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EcoOperators");
        }
    }
}
