namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LongerOperatorName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Operators", "OperatorName", c => c.String(nullable: false, maxLength: 110));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operators", "OperatorName", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
