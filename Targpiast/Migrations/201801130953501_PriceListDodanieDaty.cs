namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceListDodanieDaty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceLists", "PriceListDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PriceLists", "PriceListDateTime");
        }
    }
}
