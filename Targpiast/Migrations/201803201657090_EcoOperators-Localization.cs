namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EcoOperatorsLocalization : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EcoOperators", "Localization", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EcoOperators", "Localization", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
