namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TargpiastModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Localizations",
                c => new
                    {
                        LocalizationId = c.Int(nullable: false, identity: true),
                        LocalizationName = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.LocalizationId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsId = c.Int(nullable: false, identity: true),
                        NewsTitle = c.String(nullable: false, maxLength: 50),
                        NewsIncludeOnMain = c.Boolean(nullable: false),
                        NewsTitlePhotoId = c.String(maxLength: 40),
                        NewsShortText = c.String(maxLength: 250),
                        NewsText = c.String(),
                        NewsCreatedDate = c.DateTime(nullable: false),
                        NewsEditedDate = c.DateTime(nullable: false),
                        NewsCategory_NewsCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NewsId)
                .ForeignKey("dbo.NewsCategories", t => t.NewsCategory_NewsCategoryId, cascadeDelete: true)
                .Index(t => t.NewsCategory_NewsCategoryId);
            
            CreateTable(
                "dbo.NewsCategories",
                c => new
                    {
                        NewsCategoryId = c.Int(nullable: false, identity: true),
                        NewsCategoryName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.NewsCategoryId);
            
            CreateTable(
                "dbo.Operators",
                c => new
                    {
                        OperatorId = c.Int(nullable: false, identity: true),
                        OperatorName = c.String(nullable: false, maxLength: 50),
                        ObjectNumber = c.String(nullable: false, maxLength: 20),
                        ProductCategory = c.String(nullable: false, maxLength: 100),
                        ContactData = c.String(nullable: false, maxLength: 100),
                        Localization_LocalizationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OperatorId)
                .ForeignKey("dbo.Localizations", t => t.Localization_LocalizationId, cascadeDelete: true)
                .Index(t => t.Localization_LocalizationId);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        PhotoGalleryId = c.Int(nullable: false, identity: true),
                        PhotoGalleryName = c.String(nullable: false, maxLength: 50),
                        PhotoGalleryIncludeOnMain = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PhotoGalleryId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(nullable: false, maxLength: 30),
                        Price = c.String(nullable: false, maxLength: 20),
                        Unit = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        VideoId = c.Int(nullable: false, identity: true),
                        VideoName = c.String(nullable: false, maxLength: 50),
                        VideoIncludeOnMain = c.Boolean(nullable: false),
                        VideoUrl = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.VideoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operators", "Localization_LocalizationId", "dbo.Localizations");
            DropForeignKey("dbo.News", "NewsCategory_NewsCategoryId", "dbo.NewsCategories");
            DropIndex("dbo.Operators", new[] { "Localization_LocalizationId" });
            DropIndex("dbo.News", new[] { "NewsCategory_NewsCategoryId" });
            DropTable("dbo.Videos");
            DropTable("dbo.Products");
            DropTable("dbo.Photos");
            DropTable("dbo.Operators");
            DropTable("dbo.NewsCategories");
            DropTable("dbo.News");
            DropTable("dbo.Localizations");
        }
    }
}
