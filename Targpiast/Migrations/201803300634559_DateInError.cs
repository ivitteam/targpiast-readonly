namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateInError : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Errors", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Errors", "Date");
        }
    }
}
