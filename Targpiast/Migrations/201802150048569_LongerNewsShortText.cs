namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LongerNewsShortText : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.News", "NewsShortText", c => c.String(maxLength: 600));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.News", "NewsShortText", c => c.String(maxLength: 250));
        }
    }
}
