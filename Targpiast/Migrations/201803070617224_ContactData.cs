namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactData : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Operators", "ContactData", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operators", "ContactData", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
