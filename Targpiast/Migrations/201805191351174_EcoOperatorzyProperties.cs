namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EcoOperatorzyProperties : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EcoOperators", "OperatorName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.EcoOperators", "ProductCategory", c => c.String(nullable: false, maxLength: 120));
            AlterColumn("dbo.EcoOperators", "ContactData", c => c.String(nullable: false, maxLength: 160));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EcoOperators", "ContactData", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.EcoOperators", "ProductCategory", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.EcoOperators", "OperatorName", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
