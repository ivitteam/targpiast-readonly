namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OperatorsPoprawka : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Operators", "Localization_LocalizationId", "dbo.Localizations");
            DropIndex("dbo.Operators", new[] { "Localization_LocalizationId" });
            AlterColumn("dbo.Operators", "Localization_LocalizationId", c => c.Int());
            CreateIndex("dbo.Operators", "Localization_LocalizationId");
            AddForeignKey("dbo.Operators", "Localization_LocalizationId", "dbo.Localizations", "LocalizationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operators", "Localization_LocalizationId", "dbo.Localizations");
            DropIndex("dbo.Operators", new[] { "Localization_LocalizationId" });
            AlterColumn("dbo.Operators", "Localization_LocalizationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Operators", "Localization_LocalizationId");
            AddForeignKey("dbo.Operators", "Localization_LocalizationId", "dbo.Localizations", "LocalizationId", cascadeDelete: true);
        }
    }
}
