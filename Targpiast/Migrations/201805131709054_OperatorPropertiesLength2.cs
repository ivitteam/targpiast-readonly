namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OperatorPropertiesLength2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Operators", "ContactData", c => c.String(maxLength: 160));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operators", "ContactData", c => c.String(maxLength: 120));
        }
    }
}
