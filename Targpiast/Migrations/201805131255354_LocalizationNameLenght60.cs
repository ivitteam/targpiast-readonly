namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocalizationNameLenght60 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Localizations", "LocalizationName", c => c.String(nullable: false, maxLength: 60));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Localizations", "LocalizationName", c => c.String(nullable: false, maxLength: 30));
        }
    }
}
