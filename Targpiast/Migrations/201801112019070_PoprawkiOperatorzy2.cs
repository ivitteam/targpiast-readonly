namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PoprawkiOperatorzy2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Operators", name: "LocalizationId", newName: "Localization_LocalizationId");
            RenameIndex(table: "dbo.Operators", name: "IX_LocalizationId", newName: "IX_Localization_LocalizationId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Operators", name: "IX_Localization_LocalizationId", newName: "IX_LocalizationId");
            RenameColumn(table: "dbo.Operators", name: "Localization_LocalizationId", newName: "LocalizationId");
        }
    }
}
