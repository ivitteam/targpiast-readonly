namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OperatorPropertiesLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Operators", "OperatorName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Operators", "ObjectNumber", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Operators", "ProductCategory", c => c.String(nullable: false, maxLength: 120));
            AlterColumn("dbo.Operators", "ContactData", c => c.String(maxLength: 120));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operators", "ContactData", c => c.String(maxLength: 100));
            AlterColumn("dbo.Operators", "ProductCategory", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Operators", "ObjectNumber", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Operators", "OperatorName", c => c.String(nullable: false, maxLength: 110));
        }
    }
}
