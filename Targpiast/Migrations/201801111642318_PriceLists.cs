namespace Targpiast.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceLists : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PriceLists",
                c => new
                    {
                        PriceListId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.PriceListId);
            
            AddColumn("dbo.Products", "PriceList_PriceListId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "PriceList_PriceListId");
            AddForeignKey("dbo.Products", "PriceList_PriceListId", "dbo.PriceLists", "PriceListId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "PriceList_PriceListId", "dbo.PriceLists");
            DropIndex("dbo.Products", new[] { "PriceList_PriceListId" });
            DropColumn("dbo.Products", "PriceList_PriceListId");
            DropTable("dbo.PriceLists");
        }
    }
}
