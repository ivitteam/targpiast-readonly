﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Targpiast.Logic.Repositories;
using Targpiast.Models;

namespace Targpiast.Controllers
{
    /* Galeria */
    public class GalleryController : Controller
    {
        private readonly IVideoGalleriesRepository videoRepository;
        private readonly IPhotoGalleriesRepository photoRepository;
        private const int SizeOfThumb = 520;

        public GalleryController(IVideoGalleriesRepository objIrepository, IPhotoGalleriesRepository objIrepository2)
        {
            videoRepository = objIrepository;
            photoRepository = objIrepository2;
        }

        public ActionResult Index()
        {
            var model = photoRepository.All;
            return View(model.ToList());
        }

        #region Video

        public ActionResult Video()
        {
            return View(videoRepository.All.OrderByDescending(p => p.VideoId).ToList());
        }

        public ActionResult AddVideo(Video model)
        {
            videoRepository.InsertOrUpdate(model);
            videoRepository.Save();
            return RedirectToAction("Video");
        }

        public ActionResult DeleteVideo(int id)
        {
            videoRepository.Delete(id);
            videoRepository.Save();
            return RedirectToAction("Video");
        }

        #endregion
        [Authorize]
        public ActionResult AddPhotoGallery()
        {
            var tempPath = HostingEnvironment.MapPath("~/Content/gallery-images/upload-images");
            DirectoryInfo temp;
            
            if (tempPath != null)
            {
                temp = new DirectoryInfo(tempPath);
                if (!temp.Exists)
                {
                    Directory.CreateDirectory(tempPath);
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
            }

            foreach (var file in temp.EnumerateFiles())
            {
                file.Delete();
            }

            return View();
        }

        [HttpPost]
        public ActionResult PhotoUpload(HttpPostedFileBase file)
        {
            try
            {
                var extension = Path.GetExtension(file.FileName);
                var tempPath = HostingEnvironment.MapPath("~/Content/gallery-images/upload-images");

                DirectoryInfo temp;

                if (tempPath != null)
                {
                    temp = new DirectoryInfo(tempPath);
                    if (!temp.Exists)
                    {
                        Directory.CreateDirectory(tempPath);
                    }
                }
                else
                {
                    throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
                }

                var randomFilename = Guid.NewGuid().ToString("N");
                file.SaveAs($"{temp.FullName}\\{randomFilename}.{extension}");
            }
            catch (Exception exception)
            {
                return Json(new
                {
                    success = false,
                    response = exception.Message
                });
            }

            return Json(new
            {
                success = true,
                response = "Plika załadowany."
            });
        }


        [HttpPost]
        public ActionResult AddGallery(string title)
        {
            try
            {
                var tempPath = HostingEnvironment.MapPath("~/Content/gallery-images/upload-images");
                DirectoryInfo temp;

                if (tempPath != null)
                {
                    temp = new DirectoryInfo(tempPath);
                    if (!temp.Exists)
                    {
                        Directory.CreateDirectory(tempPath);
                    }
                }
                else
                {
                    throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
                }

                var model = new Photo
                {
                    PhotoGalleryIncludeOnMain = true,
                    PhotoGalleryName = title
                };

                photoRepository.InsertOrUpdate(model);
                photoRepository.Save();

                var galleryId = model.PhotoGalleryId;

                var galleryPath = HostingEnvironment.MapPath("~/Content/gallery-images/");

                var newGallery = Directory.CreateDirectory($"{galleryPath}{galleryId}");
                var newThumbGallery = Directory.CreateDirectory($"{newGallery.FullName}\\thumbinals");

                foreach (var file in temp.GetFiles())
                {
                    file.MoveTo($"{newGallery.FullName}\\{file.Name}");

                    #region Create Thumbinal

                    //----------        Getting the Image File
                    var img = Image.FromFile($"{newGallery.FullName}\\{file.Name}");

                    //----------        Resizing
                    img = Resize(img, SizeOfThumb);

                    //----------        Saving Image
                    img.Save($"{newThumbGallery.FullName}\\{file.Name}");

                    #endregion
                }
            }
            catch (Exception exception)
            {
                return Json(new
                {
                    success = false,
                    response = exception.Message
                });
            }

            return Json(new
            {
                success = true,
                response = "Galeria utworzona."
            });
        }
        
        public ActionResult Gallery(int id)
        {
            return View(photoRepository.Find(id));
        }

        private static Image Resize(Image originalImage, int w)
        {
            //Original Image attributes
            var originalWidth = originalImage.Width;
            var originalHeight = originalImage.Height;

            // Figure out the ratio
            var ratio = w / (double)originalWidth;
            
            // now we can get the new height and width
            var newHeight = Convert.ToInt32(originalHeight * ratio);
            var newWidth = Convert.ToInt32(originalWidth * ratio);

            Image thumbnail = new Bitmap(newWidth, newHeight);
            var graphic = Graphics.FromImage(thumbnail);

            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;

            graphic.Clear(Color.Transparent);
            graphic.DrawImage(originalImage, 0, 0, newWidth, newHeight);

            return thumbnail;
        }

        public ActionResult DeletePhotoGallery(int id)
        {
            var gallery = photoRepository.Find(id);
            var galleryPath = HostingEnvironment.MapPath($"~/Content/gallery-images/{gallery.PhotoGalleryId}/");
            if (galleryPath != null) Directory.Delete(galleryPath, true);

            photoRepository.Delete(id);
            photoRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                videoRepository.Dispose();
                photoRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}