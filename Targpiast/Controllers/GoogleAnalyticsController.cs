﻿using System;
using System.Web.Mvc;
using Targpiast.Logic;
using Targpiast.Logic.Repositories;
using Targpiast.Models;


namespace Targpiast.Controllers
{
    [Authorize]
    public class GoogleAnalyticsController : Controller
    {
        private readonly IAnalitycsChartsLogic _analitycsChartsLogic;
        private readonly IErrorRepository _errorRepository;

        public GoogleAnalyticsController(IAnalitycsChartsLogic iAnalitycsChartsLogic, IErrorRepository errorRepository)
        {
            _analitycsChartsLogic = iAnalitycsChartsLogic;
            _errorRepository = errorRepository;
        }

        // GET: GoogleAnalytics
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LineChartLastMonthVisits()
        {
            try
            {
                var result = _analitycsChartsLogic.GetDailyVisits();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                throw;
            }
        }

        [HttpPost]
        public JsonResult LineChartHourAvarageVisits()
        {
            try
            {
                var result = _analitycsChartsLogic.GetHourAvarageVisits();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                throw;
            }
        }

        [HttpPost]
        public JsonResult BarChartMostVisited()
        {
            try
            {
                var result = _analitycsChartsLogic.GetMostVisitedPages();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                throw;
            }
        }

        [HttpPost]
        public JsonResult BrowsersLastMonth()
        {
            try
            {
                var result = _analitycsChartsLogic.GetBrowsers();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                throw;
            }
        }

        [HttpPost]
        public JsonResult DevicesLastMonth()
        {
            try
            {
                var result = _analitycsChartsLogic.GetDevices();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                throw;
            }
        }
    }
}