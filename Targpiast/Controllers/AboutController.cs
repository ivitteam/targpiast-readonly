﻿using System.Web.Mvc;

namespace Targpiast.Controllers
{
    /* O firmie */
    public class AboutController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult History()
        {
            return View();
        }

        public ActionResult RegisterData()
        {
            return View();
        }
    }
}