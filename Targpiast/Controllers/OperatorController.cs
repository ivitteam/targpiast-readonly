﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Targpiast.Logic.Repositories;
using Targpiast.Models;

namespace Targpiast.Controllers
{
    /* Spis operatorów */
    public class OperatorController : Controller
    {
        private readonly IOperatorsRepository operatorsRepository;
        private readonly IErrorRepository _errorRepository;
        public OperatorController(IOperatorsRepository objIrepository, IErrorRepository errorRepository)
        {
            operatorsRepository = objIrepository;
            _errorRepository = errorRepository;
        }

        public ActionResult Index()
        {
            return View(operatorsRepository.AllLocalizationsWithOperators);
        }

        #region Localization methods

        public ActionResult AddLocalization(string locName)
        {
            operatorsRepository.InsertOrUpdate(new Localization
            {
                LocalizationName = locName
            });

            try
            {
                operatorsRepository.Save();
                return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult _Localization(Localization model)
        {
            return PartialView(model);
        }

        public void DeleteLocalization(int id)
        {
            operatorsRepository.DeleteLocalization(id);
        }

        #endregion
        
        #region Operator Methods

        public ActionResult ModifyOperators(IEnumerable<Localization> model)
        {
            foreach (var localization in model)
            {
                operatorsRepository.InsertOrUpdate(localization);
            }
            

            try
            {
                operatorsRepository.Save();
                return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NewOperator(int locId)
        {
            ViewBag.LocId = locId;
            return PartialView("EditorTemplates/NewOperator", new Operator());
        }

        public ActionResult AddOperator(Operator model, int locId)
        {
            var loc = operatorsRepository.AllLocalizationsWithOperators.Include(p => p.Operators).First(p => p.LocalizationId == locId);
            loc.Operators.Add(model);
            operatorsRepository.InsertOrUpdate(loc);
           
            try
            {
                operatorsRepository.Save();
                return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public void DeleteOperator(int id)
        {
            operatorsRepository.Delete(id);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               operatorsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}