﻿using System.Web.Mvc;

namespace Targpiast.Controllers
{
    /* Informacje praktyczne */
    public class InfoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Rules()
        {
            return View();
        }
        
        public ActionResult Downloads()
        {
            return View();
        }

        public ActionResult MarketPlan()
        {
            return View();
        }

	    public ActionResult Monitoring()
	    {
		    return View();
	    }
	}
}