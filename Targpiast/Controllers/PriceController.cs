﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Targpiast.Logic.Repositories;
using Targpiast.Models;
using Spire.Pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf.Tables;

namespace Targpiast.Controllers
{
    /* Notowania cenowe */
    public class PriceController : Controller
    {
        private readonly IPriceListsRepository _priceListsRepository;
        private readonly IErrorRepository _errorRepository;

        public PriceController(IPriceListsRepository priceListsIrepository, IErrorRepository errorRepository)
        {
            _priceListsRepository = priceListsIrepository;
            _errorRepository = errorRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (TempData["PriceListId"] != null)
            {
                var id = (int) TempData["PriceListId"];
                TempData["PriceListId"] = null;
                var allPriceLists = _priceListsRepository.AllPriceLists.OrderByDescending(p => p.PriceListDateTime);
                ViewBag.PriceListToShow = _priceListsRepository.FindPriceList(id);
                return View(allPriceLists);
            }
            else
            {
                var allPriceLists = _priceListsRepository.AllPriceLists.OrderByDescending(p => p.PriceListDateTime);
                ViewBag.PriceListToShow = _priceListsRepository.FindPriceList(allPriceLists.First().PriceListId);
                return View(allPriceLists);
            }
        }

        [HttpPost]
        public ActionResult Index(int id)
        {
            TempData["PriceListId"] = id;
            return JavaScript("location.reload(true)");
        }

        public ActionResult _PriceList(PriceList model)
        {
            return PartialView(model);
        }

        public ActionResult PriceListEditor(PriceList model)
        {
            ViewBag.PriceListId = model.PriceListId;
            return PartialView("_PriceListEditor", model.Products.OrderBy(p => p.ProductName));
        }

        public JsonResult CopyPriceList(IEnumerable<Product> model)
        {
            try
            {
                _priceListsRepository.CopyPriceList(model);
                return Json(new {OK = true}, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> {"Coś poszło nie tak spróbuj ponownie"};
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SavePriceList(IEnumerable<Product> model)
        {
            foreach (var product in model)
            {
                _priceListsRepository.InsertOrUpdate(product);
            }
            
            try
            {
                _priceListsRepository.Save();
                return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public void DeletePriceList(int id)
        {
            _priceListsRepository.DeletePriceList(id);
        }

        public void DeleteProduct(int id)
        {
            _priceListsRepository.Delete(id);
        }

        public ActionResult NewProduct(int priceListId)
        {
            ViewBag.PriceListId = priceListId;
            return PartialView("EditorTemplates/NewProduct", new Product());
        }

        public JsonResult AddProduct(Product model, int plId)
        {
            var priceList = _priceListsRepository.AllPriceLists.First(p => p.PriceListId == plId);
            priceList.Products.Add(model);
            _priceListsRepository.InsertOrUpdate(priceList);

            try
            {
                _priceListsRepository.Save();
                return Json(new { OK = true, ID = plId }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GeneratePdf(PriceList model)
        {
            //Create a pdf document.
            var doc = new PdfDocument();
            var sec = doc.Sections.Add();
            sec.PageSettings.Width = PdfPageSize.A4.Width;
            PdfPageBase page = sec.Pages.Add();
            float y = 10;

            //Title
            var brush1 = PdfBrushes.Black;
            var font1 = new PdfTrueTypeFont(new Font("Arial", 16f, FontStyle.Bold), true);
            var format1 = new PdfStringFormat(PdfTextAlignment.Center);
            var font2 = new PdfTrueTypeFont(new Font("Arial", 10f, FontStyle.Regular), true);
            page.Canvas.DrawString($"Notowania cenowe: {model.PriceListDateTime.ToShortDateString()}", font1, brush1, page.Canvas.ClientSize.Width / 2, y, format1);
            
            y = y + font1.MeasureString($"Notowania cenowe: {model.PriceListDateTime.ToShortDateString()}", format1).Height;
            y = y + 5;

            page.Canvas.DrawString("Prezentowane ceny nie stanowią oferty w rozumieniu kodeksu cywilnego.", font2, brush1, page.Canvas.ClientSize.Width / 2, y, format1);

            y = y + font2.MeasureString("Prezentowane ceny nie stanowią oferty w rozumieniu kodeksu cywilnego.", format1).Height;
            y = y + 5;

            var col = new List<string> {"Nazwa produktu;Cena;Jednostka"};
            col.AddRange(_priceListsRepository.AllPriceListsWithProducts.First(p => p.PriceListId == model.PriceListId).Products.Select(p => $"{p.ProductName};{p.Price};{p.Unit}").ToArray());
            var data = col.ToArray();

            var dataSource
                = new string[data.Length][];
            for (var i = 0; i < data.Length; i++)
            {
                dataSource[i] = data[i].Split(';');
            }


            var table = new PdfTable
            {
                Style =
                {
                    CellPadding = 2,
                    BorderPen = new PdfPen(brush1, 0.75f),
                    HeaderStyle = {StringFormat = new PdfStringFormat(PdfTextAlignment.Center)},
                    HeaderSource = PdfHeaderSource.Rows,
                    HeaderRowCount = 1,
                    ShowHeader = true,
                    DefaultStyle = new PdfCellStyle
                    {
                        Font = new PdfTrueTypeFont(new Font("Arial", 10f),true)
                    }
                }
            };
            table.Style.HeaderStyle.BackgroundBrush = PdfBrushes.LightGray;
            table.DataSource = dataSource;
            foreach (PdfColumn column in table.Columns)
            {
                column.StringFormat = new PdfStringFormat(PdfTextAlignment.Center, PdfVerticalAlignment.Middle);
            }

            table.Draw(page, new PointF(0, y));

            var stream = new MemoryStream();
            doc.SaveToStream(stream);
            doc.Close();
            stream.Position = 0;

            var result = new FileStreamResult(stream, "application/pdf");
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _priceListsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}