﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Targpiast.Logic.Repositories;
using Targpiast.Models;

namespace Targpiast.Controllers
{
    /* Aktualności */
    public class NewsController : Controller
    {
        private readonly INewsRepository newsRepository;
        private readonly IErrorRepository errorRepository;
        private const int NewsOnPage = 5;

        public NewsController(INewsRepository objIrepository, IErrorRepository objErrorRepository)
        {
            newsRepository = objIrepository;
            errorRepository = objErrorRepository;
        }

        public ActionResult Index(int? page)
        {
            if (!page.HasValue)
            {
                ViewBag.SelectedPage = 1;
                var model = newsRepository.AllTargpiastNews;
                ViewBag.PagesCount = model.Count() % NewsOnPage == 0 ? model.Count() / NewsOnPage : model.Count() / NewsOnPage + 1;

                return View(model.Take(NewsOnPage));
            }
            else
            {
                ViewBag.SelectedPage = page.Value;
                var model = newsRepository.AllTargpiastNews;
                ViewBag.PagesCount = model.Count() % NewsOnPage == 0
                    ? model.Count() / NewsOnPage
                    : model.Count() / NewsOnPage + 1;

                return View(model.Skip((page.Value - 1) * NewsOnPage).Take(NewsOnPage));
            }
        }

        public ActionResult News(int articleId)
        {
            return View(newsRepository.Find(articleId));
        }

        public ActionResult ModifyNews(News model)
        {
            if (model.NewsId != 0)
            {
                //Modify existing news
                var name = Guid.NewGuid().ToString("N");
                var fileResult = LoadTempPhotoAsNewsImage(name);
                if (fileResult != "")
                {
                    DeletePhotoById(model.NewsTitlePhotoId);
                    model.NewsTitlePhotoId = fileResult;
                }
                model.NewsCategory = newsRepository.AllCategories.SingleOrDefault(p => p.NewsCategoryName == "Aktualności");
                model.NewsEditedDate = DateTime.Now;
                newsRepository.InsertOrUpdate(model);
            }
            else
            {
                //Add new News
                model.NewsCategory = newsRepository.AllCategories.SingleOrDefault(p => p.NewsCategoryName == "Aktualności");
                model.NewsCreatedDate = DateTime.Now;
                model.NewsEditedDate = DateTime.Now;

                var name = Guid.NewGuid().ToString("N");
                var fileResult = LoadTempPhotoAsNewsImage(name);
                model.NewsTitlePhotoId = fileResult == "" ? null : fileResult;

                newsRepository.InsertOrUpdate(model);
            }


            try
            {
                newsRepository.Save();
                return Json(new { OK = true, ID = $"{model.NewsId}" }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var messages = new List<string>();
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    messages.AddRange(validationErrors.ValidationErrors.Select(validationError => Helpers.CodeHelpers.ReplaceToFriendlyNames(validationError.ErrorMessage)));
                }
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                errorRepository.InsertOrUpdate(new Error(e));
                var messages = new List<string> { "Coś poszło nie tak spróbuj ponownie" };
                return Json(new { OK = false, Errors = messages }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddNews(int? id)
        {
            News model;
            if (id != null)
            {
                model = newsRepository.Find(id);
            }
            else
            {
                model = new News
                {
                    NewsCategory = newsRepository.AllCategories.SingleOrDefault(p => p.NewsCategoryName == "Aktualności")
                };
            }


            var tempPath = HostingEnvironment.MapPath("~/Content/news-images/temp");

            DirectoryInfo temp;

            if (tempPath != null)
            {
                temp = new DirectoryInfo(tempPath);
                if (!temp.Exists)
                {
                    Directory.CreateDirectory(tempPath);
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
            }

            foreach (var file in temp.EnumerateFiles())
            {
                file.Delete();
            }

            return View(model);
        }

        public ActionResult UploadTempPhoto(HttpPostedFileBase file)
        {
            try
            {
                var extension = Path.GetExtension(file.FileName);
                var tempPath = HostingEnvironment.MapPath("~/Content/news-images/temp");

                DirectoryInfo temp;

                if (tempPath != null)
                {
                    temp = new DirectoryInfo(tempPath);
                    if (!temp.Exists)
                    {
                        Directory.CreateDirectory(tempPath);
                    }
                }
                else
                {
                    throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
                }

                var randomFilename = Guid.NewGuid().ToString("N");
                file.SaveAs($"{temp.FullName}\\{randomFilename}.{extension}");
            }
            catch (Exception exception)
            {
                return Json(new
                {
                    success = false,
                    response = exception.Message
                });
            }

            return Json(new
            {
                success = true,
                response = "Plika załadowany."
            });
        }

        private static string LoadTempPhotoAsNewsImage(string name)
        {
            
            var newsPath = HostingEnvironment.MapPath("~/Content/news-images");
            var tempPath = HostingEnvironment.MapPath("~/Content/news-images/temp");

            DirectoryInfo temp;

            if (tempPath != null)
            {
                temp = new DirectoryInfo(tempPath);
                if (!temp.Exists)
                {
                    Directory.CreateDirectory(tempPath);
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Folder tymczasowy nie znaleziony");
            }

            var file = temp.EnumerateFiles().FirstOrDefault();

            if (file == null) return "";
            
            file.MoveTo($"{newsPath}\\{name}{file.Extension}");

            return $"{name}{file.Extension}";
        }

        private static void DeletePhotoById(string name)
        {
            if (string.IsNullOrEmpty(name)) return;
            var imagePath = HostingEnvironment.MapPath($"~/Content/news-images/{name}");
            if (imagePath == null) throw new Exception("Nie udało się znaleźć pliku !");
            var file = new FileInfo(imagePath);
            file.Delete();
        }

        public ActionResult DeleteArticle(int id)
        {
            var article = newsRepository.Find(id);

            if (article == null) throw new Exception("Nie znaleziono artykułu !");

            DeletePhotoById(article.NewsTitlePhotoId);

            newsRepository.Delete(id);
            newsRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                newsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}