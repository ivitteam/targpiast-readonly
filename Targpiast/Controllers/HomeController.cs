﻿using System;
using System.Web.Mvc;
using Targpiast.Logic.Repositories;
using Targpiast.Models;

namespace Targpiast.Controllers
{
    /* Start */
    public class HomeController : Controller
    {
        private readonly IErrorRepository errorRepository;

        public HomeController(IErrorRepository errorIrepository)
        {
            errorRepository = errorIrepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult Error(Exception exception)
        {
            if (exception == null) return View();
            errorRepository.InsertOrUpdate(new Error(exception));
            return View();
        }
    }
}