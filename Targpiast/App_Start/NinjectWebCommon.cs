using Targpiast.Logic;
using Targpiast.Logic.Repositories;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Targpiast.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Targpiast.App_Start.NinjectWebCommon), "Stop")]

namespace Targpiast.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IOperatorsRepository>().To<OperatorsRepository>();
            kernel.Bind<IPhotoGalleriesRepository>().To<PhotoGalleriesRepository>();
            kernel.Bind<IVideoGalleriesRepository>().To<VideoGalleriesRepository>();
            kernel.Bind<IEcoOperatorsRepository>().To<EcoOperatorsRepository>();
            kernel.Bind<INewsRepository>().To<NewsRepository>();
            kernel.Bind<IPriceListsRepository>().To<PriceListsRepository>();
            kernel.Bind<IErrorRepository>().To<ErrorRepository>();
            kernel.Bind<IAnalitycsChartsLogic>().To<AnalitycsChartsLogic>();
        }        
    }
}
