﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Targpiast
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Controller/About

            routes.MapRoute(
                name: "struktura-i-profil",
                url: "struktura-i-profil",
                defaults: new { controller = "About", action = "Index" });

            routes.MapRoute(
                name: "historia",
                url: "historia",
                defaults: new { controller = "About", action = "History" });

            routes.MapRoute(
                name: "dane-rejestrowe",
                url: "dane-rejestrowe",
                defaults: new { controller = "About", action = "RegisterData" });

            #endregion

            #region Controller/Price
            
            routes.MapRoute(
                name: "notowania-cenowe",
                url: "notowania-cenowe",
                defaults: new { controller = "Price", action = "Index" });

            routes.MapRoute(
                name: "pobierz-cennik",
                url: "pobierz-cennik",
                defaults: new { controller = "Price", action = "GeneratePdf" });
            #endregion

            #region Controller/News

            routes.MapRoute(
                name: "wydarzenia",
                url: "wydarzenia",
                defaults: new { controller = "News", action = "Index" });

            routes.MapRoute(
                name: "wydarzenie",
                url: "wydarzenie",
                defaults: new { controller = "News", action = "News" });

            routes.MapRoute(
                name: "dodaj-wydarzenie",
                url: "dodaj-wydarzenie",
                defaults: new { controller = "News", action = "AddNews" });

            #endregion

            #region Controller/Info

            routes.MapRoute(
                name: "oplaty",
                url: "oplaty",
                defaults: new { controller = "Info", action = "Index" });

            routes.MapRoute(
                name: "regulamin",
                url: "regulamin",
                defaults: new { controller = "Info", action = "Rules" });

            routes.MapRoute(
                name: "do-pobrania",
                url: "do-pobrania",
                defaults: new { controller = "Info", action = "Downloads" });

            routes.MapRoute(
                name: "plan-rynku",
                url: "plan-rynku",
                defaults: new { controller = "Info", action = "MarketPlan" });

	        routes.MapRoute(
		        name: "monitoring-wizyjny",
		        url: "monitoring-wizyjny",
		        defaults: new { controller = "Info", action = "Monitoring" });

			#endregion

			#region Controller/Gallery

			routes.MapRoute(
                name: "foto-galeria",
                url: "foto-galeria",
                defaults: new { controller = "Gallery", action = "Index" });

            routes.MapRoute(
                name: "galeria",
                url: "galeria",
                defaults: new { controller = "Gallery", action = "Gallery" });

            routes.MapRoute(
                name: "dodaj-foto-galerie",
                url: "dodaj-foto-galerie",
                defaults: new { controller = "Gallery", action = "AddPhotoGallery" });

            routes.MapRoute(
                name: "wideo-galeria",
                url: "wideo-galeria",
                defaults: new { controller = "Gallery", action = "Video" });

            #endregion

            #region Controller/Operator

            routes.MapRoute(
                name: "spis-operatorow",
                url: "spis-operatorow",
                defaults: new { controller = "Operator", action = "Index" });

            #endregion

            #region Controller/Eco

            routes.MapRoute(
                name: "wizja-projektu",
                url: "wizja-projektu",
                defaults: new { controller = "Eco", action = "Index" });

            routes.MapRoute(
                name: "eko-wydarzenia",
                url: "eko-wydarzenia",
                defaults: new { controller = "Eco", action = "News" });

            routes.MapRoute(
                name: "eko-wydarzenie",
                url: "eko-wydarzenie",
                defaults: new { controller = "Eco", action = "SingleNews" });

            routes.MapRoute(
                name: "eko-dostawcy",
                url: "eko-dostawcy",
                defaults: new { controller = "Eco", action = "Operators" });

            routes.MapRoute(
                name: "dodaj-eco-wydarzenie",
                url: "dodaj-eco-wydarzenie",
                defaults: new { controller = "Eco", action = "AddNews" });
            #endregion

            #region Controller/Contact

            routes.MapRoute(
                name: "dane-kontaktowe",
                url: "dane-kontaktowe",
                defaults: new { controller = "Contact", action = "Index" });

            routes.MapRoute(
                name: "mapa-dojazdowa",
                url: "mapa-dojazdowa",
                defaults: new { controller = "Contact", action = "Map" });
            #endregion

            #region Controller/Account

            routes.MapRoute(
                name: "panel-administratora",
                url: "panel-administratora",
                defaults: new { controller = "Account", action = "Login" });

            routes.MapRoute(
                name: "wyloguj",
                url: "wyloguj",
                defaults: new { controller = "Account", action = "LogOff" });
            #endregion

            #region Controller/Home

            routes.MapRoute(
                name: "polityka-prywatności",
                url: "polityka-prywatności",
                defaults: new { controller = "Home", action = "PrivacyPolicy" });

            routes.MapRoute(
                name: "strona-nie-istnieje",
                url: "strona-nie-istnieje",
                defaults: new { controller = "Home", action = "Error" });

            #endregion

            #region Controller/GoogleAnalytycs

            routes.MapRoute(
                name: "statystyki",
                url: "statystyki",
                defaults: new { controller = "GoogleAnalytics", action = "Index" });

            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
