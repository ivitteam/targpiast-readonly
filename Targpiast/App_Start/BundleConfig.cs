﻿using System.Web.Optimization;

namespace Targpiast
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/core_css").Include(
                "~/Content/assets/plugins/bootstrap/css/bootstrap.css")
                .Include("~/Content/assets/css/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/theme_css").Include(
                "~/Content/assets/css/essentials.css",
                "~/Content/assets/css/layout-blog.css",
                "~/Content/assets/css/layout.css",
                "~/Content/assets/css/header-default.css",
                "~/Content/assets/css/footer-default.css",
                "~/Content/assets/css/color_scheme/red.css"));

            bundles.Add(new StyleBundle("~/Content/revolution_slider")
                .Include("~/Content/assets/css/revolution-slider.css", new CssRewriteUrlTransform())
                .Include("~/Content/assets/css/layerslider.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/core-js-jquery").Include(
                "~/Content/assets/plugins/jquery-2.2.3.min.js",
                "~/Content/assets/plugins/jquery.isotope.js",
                "~/Content/assets/plugins/masonry.js",
                "~/Content/assets/plugins/bootstrap/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/revolution-slider")
                .Include("~/Content/assets/plugins/revolution-slider/js/jquery.themepunch.tools.min.js",
                "~/Content/assets/plugins/revolution-slider/js/jquery.themepunch.revolution.min.js",
                "~/Content/assets/js/revolution_slider.js"));

            //SKY-FORMS
            bundles.Add(new StyleBundle("~/Content/sky_forms").Include(
                "~/Content/assets/css/sky-forms.css"));

            //MAGNIFIC-POPUP
            bundles.Add(new StyleBundle("~/Content/magnific_popup").Include(
                "~/Content/assets/plugins/magnific-popup/magnific-popup.css"));

            bundles.Add(new ScriptBundle("~/bundles/magnific_popup")
                .Include("~/Content/assets/plugins/magnific-popup/jquery.magnific-popup.js"));

            //DROPZONE.JS
            bundles.Add(new ScriptBundle("~/bundles/dropzone")
                .Include("~/Content/assets/plugins/dropzone/dropzone.js"));

            bundles.Add(new StyleBundle("~/Content/dropzone").Include(
                "~/Content/assets/plugins/dropzone/dropzone.css",
                "~/Content/assets/plugins/dropzone/basic.css"));

            //jQuery VALIDATION
            bundles.Add(new ScriptBundle("~/bundles/jQuerry_validation")
                .Include(
                    "~/Scripts/jquery.validate.min.js",
                    "~/Scripts/jquery.validate.unobtrusive.min.js"));

            //BUSY-LOAD
            bundles.Add(new ScriptBundle("~/bundles/busy-load")
                .Include("~/Content/assets/plugins/busy-load/busy-load.min.js"));
            bundles.Add(new StyleBundle("~/Content/busy-load").Include(
                "~/Content/assets/plugins/busy-load/busy-load.min.css"));
        }
    }
}
