﻿using System;
using System.Linq;
using Targpiast.Models;

namespace Targpiast.Logic.Repositories
{
    public interface IPhotoGalleriesRepository : IDisposable
    {
        IQueryable<Photo> All { get; }
        Photo Find(int? id);
        void InsertOrUpdate(Photo objPhotoGallery);
        void Delete(int id);
        void Save();
    }


    public class PhotoGalleriesRepository : IPhotoGalleriesRepository
    {
        private readonly ApplicationDbContext context;
        public PhotoGalleriesRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Photo> All => context.Photos.OrderByDescending(p => p.PhotoGalleryId);

        public Photo Find(int? id)
        {
            var objPhotoGallery = context.Photos.FirstOrDefault(p => p.PhotoGalleryId == id);
            return objPhotoGallery;
        }

        public void InsertOrUpdate(Photo objPhotoGallery)
        {
            if (objPhotoGallery.PhotoGalleryId == default(int))
            {
                // New entity
                context.Photos.Add(objPhotoGallery);
            }
            else
            {
                // Existing entity
                context.Entry(objPhotoGallery).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objPhotoGallery = context.Photos.Find(id);
            if (objPhotoGallery != null) context.Photos.Remove(objPhotoGallery);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}