﻿using System;
using Targpiast.Models;

namespace Targpiast.Logic.Repositories
{
    public interface IErrorRepository : IDisposable
    {
        void InsertOrUpdate(Error errorObject);
    }


    public class ErrorRepository : IErrorRepository
    {
        private readonly ApplicationDbContext context;
        public ErrorRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        
        public void InsertOrUpdate(Error errorObject)
        {
            if (errorObject.ErrorId == default(int))
            {
                // New entity
                context.Errors.Add(errorObject);
            }
            else
            {
                // Existing entity
                context.Entry(errorObject).State = System.Data.Entity.EntityState.Modified;
            }
            Save();
        }

        private void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}