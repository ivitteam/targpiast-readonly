﻿using System;
using System.Collections.Generic;
using System.Linq;
using Targpiast.Models;
using System.Data.Entity;

namespace Targpiast.Logic.Repositories
{
    public interface IPriceListsRepository : IDisposable
    {
        IQueryable<Product> All { get; }
        Product Find(int? id);
        void InsertOrUpdate(Product objProduct);
        void Delete(int id);
        IQueryable<PriceList> AllPriceLists { get; }

        IQueryable<PriceList> AllPriceListsWithProducts { get; }

        PriceList FindPriceList(int? id);
        void InsertOrUpdate(PriceList objPriceList);
        void DeletePriceList(int id);
        void Save();
        void CopyPriceList(IEnumerable<Product> productsToClone);
    }


    public class PriceListsRepository : IPriceListsRepository
    {
        private readonly ApplicationDbContext context;
        public PriceListsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Product> All => context.Products.Include(p => p.PriceList);

        public Product Find(int? id)
        {
            var objProduct = context.Products.Include(p => p.PriceList).FirstOrDefault(p => p.ProductId == id);
            return objProduct;
        }

        public void InsertOrUpdate(Product objProduct)
        {
            if (objProduct.ProductId == default(int))
            {
                // New entity
                context.Products.Add(objProduct);
            }
            else
            {
                objProduct.PriceList = context.PriceLists.FirstOrDefault(p => p.Products.Any(c => c.ProductId == objProduct.ProductId));
                context.Entry(objProduct).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objProduct = context.Products.Find(id);
            if (objProduct == null) return;
            context.Products.Remove(objProduct);
            Save();
        }

        public IQueryable<PriceList> AllPriceLists => context.PriceLists;
        public IQueryable<PriceList> AllPriceListsWithProducts => context.PriceLists.Include(p => p.Products);

        public PriceList FindPriceList(int? id)
        {
            var objPriceList = context.PriceLists.FirstOrDefault(p => p.PriceListId == id);
            return objPriceList;
        }

        public void InsertOrUpdate(PriceList objPriceList)
        {
            if (objPriceList.PriceListId == default(int))
            {
                // New entity
                context.PriceLists.Add(objPriceList);
            }
            else
            {
                // Existing entity
                context.Entry(objPriceList).State = EntityState.Modified;
            }
        }

        public void CopyPriceList(IEnumerable<Product> productsToClone)
        {
            var resultPriceList = new PriceList
            {
                PriceListDateTime = DateTime.Now,
                Products = new List<Product>()
            };

            foreach (var product in productsToClone)
            {
                resultPriceList.Products.Add(new Product
                {
                    Price = product.Price,
                    ProductName = product.ProductName,
                    Unit = product.Unit
                });
            }

            InsertOrUpdate(resultPriceList);
            Save();
        }
        
        public void DeletePriceList(int id)
        {            
            var objPriceList = AllPriceListsWithProducts.First(p => p.PriceListId == id);
            if (objPriceList == null) return;
            if (objPriceList.Products != null)
            {
                var prodCol = objPriceList.Products.Select(p => p.ProductId).ToList();
                foreach (var prodId in prodCol)
                {
                    var product = Find(prodId);
                    context.Products.Remove(product);
                }
            }
            context.PriceLists.Remove(objPriceList);
            Save();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}