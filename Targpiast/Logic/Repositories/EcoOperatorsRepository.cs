﻿using System;
using System.Linq;
using Targpiast.Models;

namespace Targpiast.Logic.Repositories
{
    public interface IEcoOperatorsRepository : IDisposable
    {
        IQueryable<EcoOperator> All { get; }
        EcoOperator Find(int? id);
        void InsertOrUpdate(EcoOperator objEcoOperator);
        void Delete(int id);
        void Save();
    }


    public class EcoOperatorsRepository : IEcoOperatorsRepository
    {
        private readonly ApplicationDbContext context;
        public EcoOperatorsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<EcoOperator> All => context.EcoOperators;

        public EcoOperator Find(int? id)
        {
            var objOperator = context.EcoOperators.FirstOrDefault(p => p.OperatorId == id);
            return objOperator;
        }

        public void InsertOrUpdate(EcoOperator objEcoOperator)
        {
            if (objEcoOperator.OperatorId == default(int))
            {
                // New entity
                context.EcoOperators.Add(objEcoOperator);
            }
            else
            {
                // Existing entity
                context.Entry(objEcoOperator).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objEcoOperator = context.EcoOperators.Find(id);
            if (objEcoOperator != null) context.EcoOperators.Remove(objEcoOperator);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}