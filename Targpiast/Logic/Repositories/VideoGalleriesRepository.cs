﻿using System;
using System.Linq;
using Targpiast.Models;

namespace Targpiast.Logic.Repositories
{
    public interface IVideoGalleriesRepository : IDisposable
    {
        IQueryable<Video> All { get; }
        IQueryable<Video> VideosOnMain { get; }
        Video Find(int? id);
        void InsertOrUpdate(Video objVideoGallery);
        void Delete(int id);
        void Save();
    }


    public class VideoGalleriesRepository : IVideoGalleriesRepository
    {
        private readonly ApplicationDbContext context;
        public VideoGalleriesRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Video> All => context.Videos;

        public IQueryable<Video> VideosOnMain => context.Videos.Where(p => p.VideoIncludeOnMain).OrderByDescending(p => p.VideoId).Take(3);

        public Video Find(int? id)
        {
            var objVideoGallery = context.Videos.FirstOrDefault(p => p.VideoId == id);
            return objVideoGallery;
        }

        public void InsertOrUpdate(Video objVideoGallery)
        {
            if (objVideoGallery.VideoId == default(int))
            {
                // New entity
                context.Videos.Add(objVideoGallery);
            }
            else
            {
                // Existing entity
                context.Entry(objVideoGallery).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objVideoGallery = context.Videos.Find(id);
            if (objVideoGallery != null) context.Videos.Remove(objVideoGallery);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}