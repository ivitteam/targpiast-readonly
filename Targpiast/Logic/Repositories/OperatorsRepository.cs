﻿using System;
using System.Linq;
using Targpiast.Models;
using System.Data.Entity;

namespace Targpiast.Logic.Repositories
{
    public interface IOperatorsRepository : IDisposable
    {
        IQueryable<Operator> All { get; }
        Operator Find(int? id);
        void InsertOrUpdate(Operator objOperator);
        void Delete(int id);
        IQueryable<Localization> AllLocalizations { get; }
        IQueryable<Localization> AllLocalizationsWithOperators { get; }
        Localization FindLocalization(int? id);
        void InsertOrUpdate(Localization objLocalization);
        void DeleteLocalization(int id);
        void Save();
    }


    public class OperatorsRepository : IOperatorsRepository
    {
        private readonly ApplicationDbContext context;
        public OperatorsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Operator> All => context.Operators;

        public Operator Find(int? id)
        {
            var objOperator = context.Operators.FirstOrDefault(p => p.OperatorId == id);
            return objOperator;
        }

        public void InsertOrUpdate(Operator objOperator)
        {
            if (objOperator.OperatorId == default(int))
            {
                // New entity
                context.Operators.Add(objOperator);
            }
            else
            {
                // Existing entity
                context.Entry(objOperator).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objOperator = context.Operators.Find(id);
            if (objOperator == null) return;
            context.Operators.Remove(objOperator);
            Save();
        }

        public IQueryable<Localization> AllLocalizationsWithOperators => context.Localizations.Include(p => p.Operators);
        public IQueryable<Localization> AllLocalizations => context.Localizations.Include(p => p.Operators);
        public Localization FindLocalization(int? id)
        {
            var objLocalization = context.Localizations.FirstOrDefault(p => p.LocalizationId == id);
            return objLocalization;
        }

        public void InsertOrUpdate(Localization objLocalization)
        {
            if (objLocalization.LocalizationId == default(int))
            {
                // New entity
                context.Localizations.Add(objLocalization);
            }
            else
            {
                // Existing entity
                context.Entry(objLocalization).State = EntityState.Modified;
                if (objLocalization.Operators == null) return;
                foreach (var oper in objLocalization.Operators)
                {
                    InsertOrUpdate(oper);
                }
            }
        }
        
        public void DeleteLocalization(int id)
        {
            var objLocalization = AllLocalizationsWithOperators.First(p => p.LocalizationId == id);
            if (objLocalization == null) return;
            if (objLocalization.Operators != null)
            {
                var operCol = objLocalization.Operators.Select(p => p.OperatorId).ToList();
                foreach (var operId in operCol)
                {
                    var oper = Find(operId);
                    context.Operators.Remove(oper);
                }
            }
            context.Localizations.Remove(objLocalization);
            Save();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}