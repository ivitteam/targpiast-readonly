﻿using System;
using System.Data.Entity;
using System.Linq;
using Targpiast.Models;

namespace Targpiast.Logic.Repositories
{
    public interface INewsRepository : IDisposable
    {
        IQueryable<News> All { get; }
        IQueryable<News> StartPageNews { get; }
        IQueryable<News> AllTargpiastNews { get; }
        IQueryable<News> AllEcoTargpiastNews { get; }
        IQueryable<NewsCategory> AllCategories { get; }
        News Find(int? id);
        void InsertOrUpdate(News objNew);
        void Delete(int id);
        void Save();
    }


    public class NewsRepository : INewsRepository
    {
        private readonly ApplicationDbContext context;
        public NewsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<News> All => context.News;

        public IQueryable<News> StartPageNews => context.News.Where(p => p.NewsIncludeOnMain).OrderByDescending(p => p.NewsCreatedDate).Take(3);

        public IQueryable<News> AllTargpiastNews => context.News.Where(p => p.NewsCategory.NewsCategoryId == 1).OrderByDescending(p => p.NewsCreatedDate);

        public IQueryable<News> AllEcoTargpiastNews => context.News.Where(p => p.NewsCategory.NewsCategoryId == 2).OrderByDescending(p => p.NewsCreatedDate);
        
        public IQueryable<NewsCategory> AllCategories => context.NewsCategories;

        public News Find(int? id)
        {
            var objNews = context.News.Include(p => p.NewsCategory).FirstOrDefault(p => p.NewsId == id);
            return objNews;
        }

        public void InsertOrUpdate(News objNews)
        {
            if (objNews.NewsId == default(int))
            {
                // New entity
                context.News.Add(objNews);
            }
            else
            {
                // Existing entity
                context.Entry(objNews).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var objNews = context.News.Find(id);
            if (objNews != null) context.News.Remove(objNews);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}