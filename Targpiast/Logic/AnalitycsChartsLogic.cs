﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Targpiast.Models;

namespace Targpiast.Logic
{
    public interface IAnalitycsChartsLogic
    {
        List<object> GetMostVisitedPages();
        List<object> GetDailyVisits();
        List<object> GetBrowsers();
        List<object> GetDevices();
        List<object> GetHourAvarageVisits();
    }

    public class AnalitycsChartsLogic : IAnalitycsChartsLogic
    {
        private const string CLIENT_ID = "172579354";
        private static readonly string[] COLORS = { "#2b80ff", "#dd2121", "#eda710", "#0dbf30", "#b00dbf", "#6bdbd3", "#e9ed2a", "#a8a1a1", "#56543d", "#cfffa8", "#ffa8e6", "#8c1a1a", "#8c5c1a", "#3d2607" };
        private static readonly string[] PAGES = { "strona-startowa", "struktura-i-profil", "historia", "dane-rejestrowe", "notowania-cenowe", "oplaty", "regulamin", "do-pobrania", "plan-rynku", "foto-galeria", "wideo-galeria", "spis-operatorow", "wizja-projektu", "eko-wydarzenia", "eko-wydarzenie", "eko-dostawcy", "dane-kontaktowe", "mapa-dojazdowa", "wydarzenia", "wydarzenie", "galeria" };

        private AnalyticsReportingService _service;
        private AnalyticsReportingService Service
        {
            get
            {
                if (_service != null) return _service;
                var path = HostingEnvironment.MapPath("~/Targpiast-d3e70ba5a308.json");
                if (path == null) throw new Exception("Nie znaleziono certyfikatu.");
                GoogleCredential credential;
                var scopes = new[] { AnalyticsReportingService.Scope.Analytics };
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    credential = GoogleCredential.FromStream(stream)
                        .CreateScoped(scopes);
                }

                // Create the service.
                _service = new AnalyticsReportingService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Targpiast GA"
                });
                return _service;
            }
        }

        public List<object> GetDailyVisits()
        {
            var data = new List<object>();
            var dtNow = DateTime.Now;
            var dateRange = new DateRange
            {
                StartDate = dtNow.AddMonths(-1).AddDays(1.0).ToString("yyyy-MM-dd"),
                EndDate = dtNow.ToString("yyyy-MM-dd")
            };

            var dimensions = new List<Dimension>
            {
                new Dimension {Name = "ga:day"}
            };

            var metrics = new List<Metric>
            {
                new Metric {Expression = "ga:sessions"},
                new Metric {Expression = "ga:users"},
                new Metric {Expression = "ga:newUsers"},
                new Metric {Expression = "ga:hits"}
            };

            var reportRequest = new ReportRequest
            {
                DateRanges = new List<DateRange> { dateRange },
                Dimensions = dimensions,
                Metrics = metrics,
                ViewId = CLIENT_ID
            };

            var result = GetDataByParameters(reportRequest);

            var dt = new DataTable();

            dt.Columns.Add("day");
            dt.Columns.Add("sessions");
            dt.Columns.Add("users");
            dt.Columns.Add("newUsers");
            dt.Columns.Add("hits");

            var colResult = (from row in result.First().Data.Rows
                let dayOfMonth = int.Parse(row.Dimensions[0])
                select new ViewsModel
                {
                    DtDay = dayOfMonth <= dtNow.Day ? new DateTime(dtNow.Year, dtNow.Month, dayOfMonth) : new DateTime(dtNow.Year, dtNow.Month - 1, dayOfMonth),
                    IntSessions = int.Parse(row.Metrics[0].Values[0]),
                    IntUsers = int.Parse(row.Metrics[0].Values[1]),
                    IntNewUsers = int.Parse(row.Metrics[0].Values[2]),
                    IntHits = int.Parse(row.Metrics[0].Values[3])
                }).ToList();

            foreach (var row in colResult.OrderBy(p => p.DtDay))
            {
                var dataRow = dt.NewRow();
                dataRow["day"] = row.DtDay.ToString("MMM dd");
                dataRow["sessions"] = row.IntSessions;
                dataRow["users"] = row.IntUsers;
                dataRow["newUsers"] = row.IntNewUsers;
                dataRow["hits"] = row.IntHits;
                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> GetHourAvarageVisits()
        {
            var data = new List<object>();
            var dtNow = DateTime.Now;
            var dateRange = new DateRange
            {
                StartDate = dtNow.AddMonths(-1).AddDays(1.0).ToString("yyyy-MM-dd"),
                EndDate = dtNow.ToString("yyyy-MM-dd")
            };

            var dimensions = new List<Dimension>
            {
                new Dimension {Name = "ga:hour"}
            };

            var metrics = new List<Metric>
            {
                new Metric {Expression = "ga:hits"}
            };

            var reportRequest = new ReportRequest
            {
                DateRanges = new List<DateRange> { dateRange },
                Dimensions = dimensions,
                Metrics = metrics,
                ViewId = CLIENT_ID
            };

            var result = GetDataByParameters(reportRequest).ToArray();

            var dt = new DataTable();

            dt.Columns.Add("hour");
            dt.Columns.Add("hits");

            foreach (var row in result.First().Data.Rows)
            {
                var dataRow = dt.NewRow();
                dataRow["hour"] = row.Dimensions[0];
                dataRow["hits"] = int.Parse(row.Metrics[0].Values[0]) / result.First().Data.Rows.Count;
                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> GetBrowsers()
        {
            var data = new List<object>();
            var dtNow = DateTime.Now;
            var dateRange = new DateRange
            {
                StartDate = dtNow.AddMonths(-1).AddDays(1.0).ToString("yyyy-MM-dd"),
                EndDate = dtNow.ToString("yyyy-MM-dd")
            };

            var dimensions = new List<Dimension>
            {
                new Dimension {Name = "ga:browser"}
            };

            var metrics = new List<Metric>
            {
                new Metric {Expression = "ga:sessions"}
            };

            var reportRequest = new ReportRequest
            {
                DateRanges = new List<DateRange> { dateRange },
                Dimensions = dimensions,
                Metrics = metrics,
                ViewId = CLIENT_ID
            };

            var result = GetDataByParameters(reportRequest).ToArray();
            var colBrowsers = result.First().Data.Rows.Select(row => new NameValueModel {Name = row.Dimensions[0], Views = int.Parse(row.Metrics[0].Values[0])}).ToList();
            var resultBrowsers = colBrowsers.OrderByDescending(p => p.Views).Take(4).ToList();
            resultBrowsers.Add(new NameValueModel
            {
                Name = "Pozostałe",
                Views = colBrowsers.OrderByDescending(p => p.Views).Skip(4).Sum(p => p.Views)
            });
            var dt = new DataTable();

            dt.Columns.Add("browser");
            dt.Columns.Add("users");
            dt.Columns.Add("colors");

            var i = 0;
            foreach (var row in resultBrowsers)
            {
                var color = i >= COLORS.Length ? COLORS[i % COLORS.Length] : COLORS[i]; 
                var dataRow = dt.NewRow();
                dataRow["browser"] = row.Name;
                dataRow["users"] = row.Views;
                dataRow["colors"] = color;
                dt.Rows.Add(dataRow);
                i++;
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> GetDevices()
        {
            var data = new List<object>();
            var dtNow = DateTime.Now;
            var dateRange = new DateRange
            {
                StartDate = dtNow.AddMonths(-1).AddDays(1.0).ToString("yyyy-MM-dd"),
                EndDate = dtNow.ToString("yyyy-MM-dd")
            };

            var dimensions = new List<Dimension>
            {
                new Dimension {Name = "ga:deviceCategory"}
            };

            var metrics = new List<Metric>
            {
                new Metric {Expression = "ga:sessions"}
            };

            var reportRequest = new ReportRequest
            {
                DateRanges = new List<DateRange> { dateRange },
                Dimensions = dimensions,
                Metrics = metrics,
                ViewId = CLIENT_ID
            };

            var result = GetDataByParameters(reportRequest);

            var dt = new DataTable();

            dt.Columns.Add("deviceCategory");
            dt.Columns.Add("users");
            dt.Columns.Add("colors");

            var i = 0;
            foreach (var row in result.First().Data.Rows)
            {
                var color = i >= COLORS.Length ? COLORS[i % COLORS.Length] : COLORS[i];
                var dataRow = dt.NewRow();
                dataRow["deviceCategory"] = row.Dimensions[0].Replace("desktop", "Komputer").Replace("tablet", "Tablet").Replace("mobile", "Smartfon");
                dataRow["users"] = row.Metrics[0].Values[0];
                dataRow["colors"] = color;
                dt.Rows.Add(dataRow);
                i++;
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> GetMostVisitedPages()
        {
            var data = new List<object>();
            var dtNow = DateTime.Now;
            var dateRange = new DateRange
            {
                StartDate = dtNow.AddMonths(-1).AddDays(1.0).ToString("yyyy-MM-dd"),
                EndDate = dtNow.ToString("yyyy-MM-dd")
            };

            var dimensions = new List<Dimension>
            {
                new Dimension {Name = "ga:pagePath"}
            };

            var metrics = new List<Metric>
            {
                new Metric {Expression = "ga:pageviews"}
            };

            var reportRequest = new ReportRequest
            {
                DateRanges = new List<DateRange> { dateRange },
                Dimensions = dimensions,
                Metrics = metrics,
                ViewId = CLIENT_ID
            };

            var result = GetDataByParameters(reportRequest);

            var colPages = PAGES.Select(page => new NameValueModel {Name = page}).ToList();
            
            foreach (var row in result.First().Data.Rows)
            {
                if (row.Dimensions[0] == "/")
                    colPages.First(p => p.Name == "strona-startowa").Views += int.Parse(row.Metrics[0].Values[0]);
                else if (colPages.Any(p => row.Dimensions[0].Contains(p.Name)))
                    colPages.First(p => row.Dimensions[0].Contains(p.Name)).Views += int.Parse(row.Metrics[0].Values[0]);
            }

            var wydarzenie = colPages.Single(p => p.Name == "wydarzenie");
            colPages.Single(p => p.Name == "wydarzenia").Views += wydarzenie.Views;
            colPages.Remove(wydarzenie);

            var ecowydarzenie = colPages.Single(p => p.Name == "eko-wydarzenie");
            colPages.Single(p => p.Name == "eko-wydarzenia").Views += ecowydarzenie.Views;
            colPages.Remove(ecowydarzenie);

            var galeria = colPages.Single(p => p.Name == "galeria");
            colPages.Single(p => p.Name == "foto-galeria").Views += galeria.Views;
            colPages.Remove(galeria);

            var resultPages = colPages.OrderByDescending(p => p.Views).Take(9).ToList();

            var pozostale = new NameValueModel
            {
                Name = "pozostałe",
                Views = colPages.OrderByDescending(p => p.Views).Skip(9).Sum(p => p.Views)
            };
            
            var dt = new DataTable();

            dt.Columns.Add("pagePath");
            dt.Columns.Add("pageviews");

            foreach (var row in resultPages.OrderByDescending(p => p.Views))
            {
                var dataRow = dt.NewRow();
                dataRow["pagePath"] = row.Name;
                dataRow["pageviews"] = row.Views;
                dt.Rows.Add(dataRow);
            }

            var lastRow = dt.NewRow();
            lastRow["pagePath"] = pozostale.Name;
            lastRow["pageviews"] = pozostale.Views;
            dt.Rows.Add(lastRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        private IEnumerable<Report> GetDataByParameters(ReportRequest request)
        {
            var getReportsRequest = new GetReportsRequest
            {
                ReportRequests = new List<ReportRequest> { request }
            };
            var batchRequest = Service.Reports.BatchGet(getReportsRequest);
            var response = batchRequest.Execute();
            return response.Reports;
        }
    }
}