﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Targpiast.Helpers
{
    public static class CodeHelpers
    {

        public static string ReplaceContactTags(string data)
        {
            if (string.IsNullOrEmpty(data)) return data;

            const string wwwRegex = @"(\[www=)((\S)+)(\])";
            const string emailRegex = @"(\[email=)((\S)+)(\])";
            const string phoneRegex = @"(\[phone=)((\S)+)(\])";
	        const string faxRegex = @"(\[fax=)((\S)+)(\])";

			var result = data;
	        var contacts = new List<string>();

            foreach (Match match in Regex.Matches(data, wwwRegex))
            {
                var strWww = match.Value.Replace("[www=", "").Replace("]", "");
                contacts.Add(string.Format(@"<a href=""{0}"" target=""_blank"">www: {0}</a>", strWww));

            }

            foreach (Match match in Regex.Matches(data, emailRegex))
            {
                var strEmail = match.Value.Replace("[email=", "").Replace("]", "");
	            contacts.Add(string.Format(@"<a href=""mailto: {0}"">e-mail: {0}</a>", strEmail));
            }

            foreach (Match match in Regex.Matches(data, phoneRegex))
            {
                var strPhone = match.Value.Replace("[phone=", "").Replace("]", "");
	            contacts.Add(string.Format(@"<a href=""tel:{0}"">tel: {0}</a>", strPhone));
            }

	        foreach (Match match in Regex.Matches(data, faxRegex))
	        {
		        var strFax = match.Value.Replace("[fax=", "").Replace("]", "");
		        contacts.Add(string.Format(@"<a href=""fax:{0}"">fax: {0}</a>", strFax));
	        }

			if (contacts.Count == 0) { return result;}

	        result = contacts[0];

	        for (var i = 1; i < contacts.Count; i++)
	        {
		        result += $"<br/>{contacts[i]}";
	        }

	        return result;
        }

        public static string ReplaceToFriendlyNames(string sourceString)
        {

            var dict = new Dictionary<string, string>
            {
                {"ProductName", "Nazwa produktu"},
                {"Price", "Cena brutto"},
                {"Unit", "Jednostka"},
                {"NewsTitle", "Tytuł"},
                {"NewsShortText", "Skrócona treść wiadomości"},
                {"NewsText", "Treść wiadomości"},
                {"LocalizationName", "Nazwa lokalizacji" },
                {"OperatorName", "Nazwa operatora" },
                {"ObjectNumber", "Numer stanowiska" },
                {"ProductCategory", "Asortyment" },
                {"ContactData", "Dane kontaktowe" }
            };

            foreach (var val in dict)
            {
                sourceString = sourceString.Replace(val.Key, val.Value);
            }

            return sourceString;
        }
    }
}