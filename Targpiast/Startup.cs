﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Targpiast.Startup))]
namespace Targpiast
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
